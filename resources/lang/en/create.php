<?php

	return [

		'next' => 'Next',
		'back' => 'Back',

		'error-title' => 'Error notification',
		'error-message' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo.',

		'step-1' => [
			'title' => 'Give us a few details about yourself',
			'info' => 'Use <strong>Facebook</strong> or <strong>Twitter</strong> to autofill your information',
			'login-with-facebook' => 'Login with Facebook',
			'login-with-twitter' => 'Login with Twitter',
			'user-photo-upload' => '<strong>Choose</strong> image or <strong>drag</strong> image to upload',

			'first-name' => 'First name',
			'last-name' => 'Last name',
			'email' => 'Email address',
			'nickname' => 'Nickname'
		],

		'step-2' => [
			'title' => 'Tell us about your memory',
			'memory-photo-upload' => '<strong>Choose</strong> image or <strong>drag</strong> image to upload',
			'info' => '<strong>We recommend to enter your memory details in English.</strong> This way it can be processed quicker as every entry will be reviewd first.',
			'name' => 'Memory name',
			'description' => 'Tell us more about your memory...',
			'location' => 'Location',
			'date' => 'Memory date',
			'tags' => 'Add some tags (e.g. #surfing #beach #bali)',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'country-code' => 'Country code',
			'country' => 'Country',

			'notifications' => 'Want to be kept up to date?',
			'notifications-info' => 'Choose all the <strong>Email notifications</strong> you want to receive',

			'option-same-country' => 'New memory from the same country',
			'option-same-year' => 'New memory from the same year',
			'option-comments' => 'New comments on your memory',
			'option-same-tags' => 'New memory with same tags',

			'option-terms' => 'I accept the Terms &amp; Conditions',
			'option-newsletter' => 'I want to receive the Lufthansa Newsletter',

		],

		'step-3' => [
			'title' => 'Thank you, for submitting your memory.',
			'thank-you-info' => 'Your memory is now under review. You\'ll receive a Email confirmation once your memory is processed.',
			'view' => 'Save',
		],

		'validations' => [
			'firstname' => [
				'required' => 'The first name field is required.',
				'size' => 'The first name must be less than 20 characters'
			],
			'lastname' => [
				'required' => 'The last name field is required.',
				'size' => 'The last name must be less than 20 characters'
			],
			'nickname' => [
				'required' => 'The nickname field is required.',
				'size' => 'The nickname name must be less than 20 characters'
			],
			'email' => [
				'required' => 'The email field is required.',
				'email' => 'The email must be a valid email address.'
			],
			'memoryname' => [
				'required' => 'The memory name field is required.',
				'size' => 'The first name must be less than 250 characters'
			],
			'description' => [
				'required' => 'The description field is required.',
				'size' => 'The description must be less than 1000 characters'
			],
			'location' => [
				'required' => 'The location field is required.',
			],
			'date' => [
				'required' => 'The date field is required.',
			],
			'file' => [
				'invalid' => 'An error ocurred while upload the file',
				'success' => 'The image has been uploaded successfully',
				'size' => 'The image is to big <strong>{{filesize}}</strong>MB.<br/>The maximum file size is <strong>{{maxFilesize}}</strong>MB',
				'type' => 'You can\'t upload files of this type.<br/>Just image <strong>jpg</strong> or <strong>png</strong> type is accepted.',
			],
			'step-2' => [
				'no-person' => 'An error occurred. Please start from the beginning!',
				'no-memory' => 'An error occurred. Please start from the beginning!'
			]
		],

	];