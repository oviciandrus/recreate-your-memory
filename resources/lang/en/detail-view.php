<?php

return [
	'connected-title' => 'Connected memories',
	'edt-comment' => 'Write your comment...',
	'send-comment' => 'Post comment',
	'show-more' => 'Show more comments',


	'validations' => [
		'firstname' => [
			'required' => 'The first name field is required.'
		],
		'lastname' => [
			'required' => 'The last name field is required.'
		],
		'nickname' => [
			'required' => 'The nickname field is required.'
		],
		'email' => [
			'required' => 'The email field is required.',
			'email' => 'The email must be a valid email address.'
		],
		'comment' => [
			'required' => 'The comment field is required.'
		]
	]
];