<?php

	return [

		'number-of-nodes' => '<strong>:count</strong> memories found',
		'add-memory-tooltip' => 'Create your memory &amp; win',

		'filter' => [
			'search' => 'Search',
			'search-placeholder' => '(e.g. #mountaineer)',

			'year' => 'Year: ',
			'continent' => 'Continent: ',
			'select-continent' => 'Select a continent',
			'country' => 'Country: ',
			'select-country' => 'Select a country',

			'apply' => 'Apply Filters',
			'reset' => 'Reset Filters',
			'cancel' => 'Cancel',

			'error' => 'Please select at least on filter criteria'
		],

	];