<?php

return [

	'language' => 'Language',

	'options' => [
		'create' => 'Create your memory',
		'about' => 'About',
		'neuronal-view' => 'Neuronal view',
		'map-view' => 'Map view',
		'win' => 'Win / Prizes',

		'terms' => 'Terms &amp; conditions',
		'imprint' => 'Imprint'
	],

	'languages' => [
		'en' => 'English',
		'it' => 'Italian',
		'es' => 'Spanish',
		'pt' => 'Portugese',
		'fr' => 'French',
		'pl' => 'Polish',
		'ru' => 'Russian',
	],

];