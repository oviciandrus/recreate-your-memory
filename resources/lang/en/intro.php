<?php

return [
	'welcome' => 'Welcome!',
	'intro' => 'Take a second to learn more about the most important features before exploring our world of memories.',
	'learn-more' => 'Learn more',
	'skip' => 'Skip',


	'tips' => [

		'2' => [
			'title' => 'This is a memory',
			'info' => 'As you will see there are a lot of memories to explore. Click on a memory to reveal the full story or to see the connections to other memories created by our users.'
		],

		'3' => [
			'title' => 'Explore the connections',
			'info' => 'There are many memories that have similarities. It can be the same location and location or even the same experience! Let the connections guide you to find out.'
		],

		'4' => [
			'title' => 'Get more in the details',
			'info' => 'Use the Search &amp; Filter tab to find memories with specific dates, locations or even hashtags.'
		],


		'5' => [
			'title' => 'Now go on and create your own memory',
			'info' => 'Tell us about a special experience and enter for your chance to win.',
			'btn' => 'Get started',
		]
	]

];