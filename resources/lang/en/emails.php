<?php

return [

	'thank-you-email' => [
		'view-in-browser' => 'View email in browser',
		'thank-you' => 'Thank you, :username!',
		'in-network' => 'Your memory is now part of the Lufthansa MemoryNetwork.',
		'view-memory' => 'View my memory',
		'change-settings' => 'Change notification settings',
		'footer-info' => ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam bibendum, dui nec mattis pulvinar, tellus ante iaculis leo, id laoreet mauris justo sit amet orci. Sed fermentum nulla vitae elit ornare, a ultricies nulla venenatis. Donec ullamcorper, lacus id sollicitudin eleifend, enim eros consectetur enim, et sodales felis urna auctor felis. Maecenas dui nulla, suscipit ut hendrerit nec, lacinia sit amet risus. Phasellus facilisis velit dui, ut blandit eros suscipit vel. Ut nec lectus purus. Etiam venenatis auctor nisl, eget viverra orci ultrices vitae. Cras ac augue vel ex tincidunt tristique. Nam ut interdum lectus, sit amet congue urna. In porta tristique ligula et rutrum. Phasellus maximus malesuada semper.

Pellentesque consequat metus ac nisl aliquet varius. Aliquam lacinia viverra mauris sit amet tempor. Morbi cursus libero ut aliquam ullamcorper. Suspendisse potenti. Sed pulvinar varius dui non pulvinar. Etiam et lectus porttitor, lacinia ligula at, sollicitudin lectus. Curabitur pharetra, urna sit amet finibus sagittis, odio lorem dignissim ipsum, ac dictum ipsum ante malesuada nisi. Maecenas porttitor dolor consequat, convallis felis eu, eleifend nisi.

Nam dignissim ipsum massa. Maecenas varius nulla non diam convallis lobortis. Praesent vel enim vel erat mollis fringilla sit amet non mauris. Cras faucibus commodo accumsan. Aenean pulvinar rhoncus turpis ac scelerisque. Cras tincidunt dapibus elit ut tincidunt. Maecenas quis nulla ut purus semper auctor. Nunc iaculis velit lobortis hendrerit molestie. Donec pretium et odio eu efficitur. Pellentesque nec nunc aliquam, iaculis diam sit amet, pharetra tellus. Curabitur tincidunt convallis tempor. Duis id hendrerit quam, efficitur pretium dolor.'
	],
];