@if( $comments->count() > 0)
	<div id="comments-small-container">
		<div id="comments-list-small-container">
			
			@include('detail-view._comments-page-small', ['comments' => $comments, 'k' => $comments->count()])
			
			@if( $next_comments_url )
				<button id="btn-small-show-more-comments" class="btn-no bk-full btn-rym btn-show-more-comments" data-next-page-url="{{ $next_comments_url }}">
					<span class="indicator indicator-blue"></span>
					<span class="caption">{{ trans('detail-view.show-more') }}</span>
				</button>
			@endif
		</div>
	</div>
@endif