<div id="memory-actions-small">

	<div class="memory-action">
		<!-- Like button -->
		{!! 
		Html::image('assets/small/like.png', 'action-like', ['class' => 'btn-like' . ( $is_liked ? ' is-liked no-pointer' : ''), 'data-memory-id' => $memory->id]) 
		!!}
	</div>

	<div class="memory-action">
		{!! Html::image('assets/small/plus.png', 'action-like', ['class' => 'btn-create-memory', 'data-url' => route('create')]) !!}
	</div>

	<div class="memory-action">
		<!-- Share on facebook -->
		{!! 
		Html::image('assets/small/facebook.png', 'action-like', [
			'class' => 'share',
			'data-share' => 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode(URL::current())
			/*
			'data-share' => 'https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fsitepoint.com%2F'
			*/
		]) 
		!!}
	</div>

	<div class="memory-action">
		<!-- Share on Twitter -->
		{!! 
		Html::image('assets/small/twitter.png', 'action-like', [
			'class' => 'share',
			'data-share' => 'https://twitter.com/intent/tweet?url=' . urlencode(URL::current()) . '&text=' . $memory->name . '&hashtags=' . $memory->tags_list
		]) 
		!!}
	</div>

	<div class="clearfix"></div>
</div>