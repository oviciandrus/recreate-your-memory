<div class="user-avatar">
	{!! Html::image($photos['user60'], $memory->nickname, ['class' => 'img-circle']) !!}
</div>

<div class="user-infos">
	<div class="user-nickname">{{ $memory->nickname }}</div>
	<div class="user-created-at">{{ $memory->to_created_at }}</div>
</div>

<div class="clearfix"></div>

<div class="memory-description">
	{{ $memory->description }}
</div>

<div class="memory-tags">
	{{ $memory->tags_to_list }}
</div>