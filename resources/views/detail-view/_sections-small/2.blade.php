<div class="memory-name">{{ $memory->name }}</div>
<div class="memory-location">{!! $memory->location !!}, {!! $memory->date_of !!}</div>
<div class="memory-likes-comments">
	<span class="memory-likes">{!! $memory->number_of_likes !!}</span>
	<span class="memory-comments">{!! $memory->number_of_comments !!}</span>
</div>