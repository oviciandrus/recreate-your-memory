<div id="comment-add-small">

	<form class="comment-posting" name="comment-posting" class="form-validation" method="post">
		<!-- First name -->
		<div class="form-group form-group-first-name">
			<input type="text" class="form-control form-user" id="txtFirstName" name="txtFirstName" placeholder="{{ trans('create.step-1.first-name') }}">
		</div>

		<!-- Last name -->
		<div class="form-group form-group-last-name">
			<input type="text" class="form-control form-user" id="txtLastName" name="txtLastName" placeholder="{{ trans('create.step-1.last-name') }}">
		</div>

		<!-- Email -->
		<div class="form-group form-group-email">
			<input type="text" class="form-control form-user" id="txtEmail" name="txtEmail" placeholder="{{ trans('create.step-1.email') }}">
		</div>

		<!-- Nickname -->
		<div class="form-group form-group-nickname">
			<input type="text" class="form-control form-user" id="txtNickname" name="txtNickname" placeholder="{{ trans('create.step-1.nickname') }}">
		</div>

		<!-- memory description -->
		<div class="form-group form-group-description">
			<textarea class="form-control form-memory" id="txtComment" name="txtComment" placeholder="{{ trans('detail-view.edt-comment') }}"></textarea>
		</div>

		<button type="submit" class="btn-no bk-full btn-rym btn-rym-blue btn-send-comment" id="btn-send-small-comment">
			<span class="indicator indicator-white"></span>
			<span class="caption">{{ trans('detail-view.send-comment') }}</span>
		</button>
	</form>

</div>