<div class="comments-page" data-page="{{$comments->currentPage()}}">
	@foreach($comments as $i => $comment)
		<div class="comment-item-container {{ $i == $k-1 ? 'last-comment' : ''}}">
			<div class="comment-item-left">
				<div class="l-c-user">
					<div class="l-c-nickname">{{ $comment->nickname }}</div>
					<div class="l-c-created-at">{{ $comment->to_created_at }}</div>
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="comment-item-right">
				{{ $comment->comment }}
			</div>

			<div class="clearfix"></div>
		</div>
	@endforeach
</div>