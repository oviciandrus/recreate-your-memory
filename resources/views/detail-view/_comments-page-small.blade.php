<div class="comments-page" data-page="{{$comments->currentPage()}}">
	@foreach($comments as $i => $comment)
		<div class="comment-item-container {{ $i == $k-1 ? 'last-comment' : ''}}">
			
			<div class="user-infos-comment-row">

				<div class="user-comment-nickname">
					{{ $comment->nickname }}
				</div>

				<div class="comment-created-at">
					{{ $comment->to_created_at }}
				</div>

				<div class="clearfix"></div>
			</div>

			<div class="comment-body">
				{{ $comment->comment }}						
			</div>
		</div>
	@endforeach
</div>