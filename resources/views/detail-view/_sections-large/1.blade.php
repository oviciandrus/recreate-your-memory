<div id="section-1-large-container">

	<div id="memory-image-large">
		{!! Html::image($photos['photo480'], 'memory-photo') !!}
	</div>

	<div id="memory-infos-large">
		<div class="memory-name">{{ $memory->name }}</div>
		<span class="memory-location">{!! $memory->location !!}, {!! $memory->date_of !!}</span>
		<span class="memory-likes-comments">
			<span class="memory-likes">{!! $memory->number_of_likes !!}</span>
			<span class="memory-comments">{!! $memory->number_of_comments !!}</span>
		</span>
	</div>

	<div class="clearfix"></div>
</div>