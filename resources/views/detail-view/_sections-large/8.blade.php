<div id="section-8-large-container">
	<h2>{{ trans('detail-view.connected-title') }}</h2>
	<div id="section-8-large-connected-container">
		<div id="connection-list">
			@foreach($connected as $i => $edge)
				<div class="connection-item">
					<div class="photo-container">
						@if($memory->id == $edge->source_id)
							{!! $edge->target->photoConnenctRender($edge) !!}
						@else
							{!! $edge->source->photoConnenctRender($edge) !!}
						@endif
					</div>
				</div>
			@endforeach
			@if($populars)
				@foreach($populars as $i => $memory)
				<div class="connection-item">
					<div class="photo-container">
						{!! $memory->photoConnenctRender(null) !!}
					</div>
				</div>
			@endforeach
			@endif
			<div style="clear:both"></div>
		</div>
	</div>
</div>