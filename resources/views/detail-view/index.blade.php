@extends('~layouts.master')

@section('styles')
	{!! Html::style('vendors/formvalidation/dist/css/formValidation.min.css') !!}
	{!! Html::style('css/detail-view.css') !!}
@stop

@section('scripts')
	<script>
		window.photos = {!! json_encode($photos) !!};
		window.memory = {!! $memory !!};
		window.validations = {!! json_encode(trans('detail-view.validations')) !!};
	</script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2X74xWumQ1zt5WgBagWAszEpYJqC1nEU"></script>
	{!! Html::script('js/google-map/markerclusterer.js') !!}
	{!! Html::script('vendors/comptech/Collection.js') !!}
	{!! Html::script('js/google-map/mapview.js') !!}
	{!! Html::script('js/google-map/map-options.js') !!}
	{!! Html::script('js/google-map/jquery.mapview.js') !!}
	{!! Html::script('js/detail-view/share.js') !!}
	{!! Html::script('vendors/formvalidation/dist/js/formValidation.min.js') !!}
	{!! Html::script('vendors/formvalidation/dist/js/framework/bootstrap.min.js') !!}
	{!! Html::script('vendors/scroll-to/jquery-scrollto.js') !!}
	{!! Html::script('js/detail-view/rym-detail-view.js') !!}
	{!! Html::script('js/detail-view/run.js') !!}
@stop

@section('content')
	
	<input id="memory-id" type="hidden" value="{{$memory->id}}" />
	
	<div class="section-container for-small">
		@foreach([1, 2, 3, 4, 5, 6, 7, 8] as $i => $section)
			<div class="detail-section detail-section-{{$section}}">
				@include('detail-view._sections-small.' . $section)
			</div>
		@endforeach
	</div>

	<div class="section-container for-large">
		@foreach([1, 2, 4, 6, 7, 8] as $i => $section)
			<div class="detail-section detail-section-{{$section}}">
				@include('detail-view._sections-large.' . $section)
			</div>
		@endforeach
	</div>
	@include('detail-view._sections-large._actions')
@stop