{!! Html::image($memory->createPhoto(320, 320, 'M'), $memory->slug_name, ['class' => 'img-responsive img-connection']) !!}

<div class="connection-infos">
	<a href="{{ route('detail-view', ['memory_id' => $memory->id, 'slug_name' => $memory->slug_name]) }}" class="name">{{$memory->name}}</a>
	<div>
		<span class="location">{!! $memory->location !!}</span>
		<span class="date-of">{!! $memory->date_of !!}</span>
	</div>
	<div>
		<span class="likes">{!!$memory->number_of_likes!!}</span>
		<span class="comments">{!!$memory->number_of_comments!!}</span>
	</div>
</div>

<div class="connection-circles">
	@if($edge)
		@if($edge->year)
			<div>
				{!! Html::image('assets/small/yellow.png', 'yellow') !!}
			</div>
		@endif
		@if($edge->country_id)
			<div>
				{!! Html::image('assets/small/blue.png', 'blue') !!}
			</div>
		@endif
		@if($edge->tags)
			<div>
				{!! Html::image('assets/small/white.png', 'white') !!}
			</div>
		@endif
	@endif
</div>