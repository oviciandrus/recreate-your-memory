@extends('~layouts.master')

@section('styles')
    {!! Html::style('vendors/ion.rangeSlider/css/normalize.css') !!}
    {!! Html::style('vendors/ion.rangeSlider/css/ion.rangeSlider.css') !!}
    {!! Html::style('vendors/ion.rangeSlider/css/ion.rangeSlider.skinHTML5.css') !!}
    {!! Html::style('vendors/tooltipster/css/tooltipster.bundle.min.css') !!}    

    {!! Html::style('css/map.css') !!}
@stop

@section('scripts')    
    {!! Html::script('js/network-view/rym-neuronal-view.js') !!}
    {!! Html::script('vendors/ion.rangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js') !!}
    {!! Html::script('vendors/tooltipster/js/tooltipster.bundle.min.js') !!}  
    @include('network-view._imports')
    {!! Html::script('js/network-view/renderers/renderer-nodes.js') !!}
    {!! Html::script('js/network-view/renderers/renderer-edges.js') !!}
    {!! Html::script('js/network-view/network-view-module.js') !!}
    {!! Html::script('js/network-view/run.js') !!}
@stop

@section('content')
    @include('~layouts._partials._map')
@stop