@extends('admin.~master.index')

@section('page-title')
	<div class="col-lg-12">
		<h1 class="page-header">RYM Dashboard</h1>
	</div>
@stop

@section('page-wrapper')
	@include('admin.dashboard._partials._statistics')
@stop