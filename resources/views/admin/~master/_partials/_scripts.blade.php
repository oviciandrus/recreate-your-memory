{!! Html::script('vendors/jquery/jquery.js') !!}
{!! Html::script('vendors/bootstrap/js/bootstrap.min.js') !!}
{!! Html::script('vendors/metisMenu/metisMenu.min.js') !!}
{!! Html::script('admin/js/comptech-soft.js') !!}
<script>
$(document).ready(function(){
	var loading = false;
	$.ajaxSetup({
    	type : 'post', 
    	data : { _token:ComptechSoft.Token()}, 
    	dataType:'json',
    	beforeSend : function()
	    {
	        // loading = true;
	        // setTimeout(function(){
	        //     if(loading) 
	        //     {
	        //         $('#spinner').show();
	        //     }
	        // }, 50);    
	    },
	    complete : function()
	    {
	        // loading = false;
	        // $('#spinner').hide();
	    }
	});
});
</script>
@yield('page-scripts')
{!! Html::script('vendors/sb-admin/js/sb-admin-2.js') !!}