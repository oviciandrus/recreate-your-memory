<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="_url" content="{{ URL::to('/')}}"/>
<meta name="_token" content="{{ csrf_token() }}"/>
<title>Recreate Your Memory - Moderation Panel</title>
{!! Html::style('vendors/bootstrap/css/bootstrap.min.css') !!}
{!! Html::style('vendors/metisMenu/metisMenu.min.css') !!}
{!! Html::style('vendors/font-awesome/css/font-awesome.min.css') !!}
@yield('page-styles')
{!! Html::style('vendors/sb-admin/css/sb-admin-2.css') !!}
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->