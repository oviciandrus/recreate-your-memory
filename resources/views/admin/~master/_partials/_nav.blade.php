<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{route('admin-dashboard')}}">RYM</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        @if(0)
        @include('admin.~master._partials._nav_messages')
        @include('admin.~master._partials._nav_tasks')
        @include('admin.~master._partials._nav_alerts')
        @endif
        @include('admin.~master._partials._nav_user')
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                
                @if(0)
                    @include('admin.~master._partials._nav_side_search')
                @endif
                
                @include('admin.~master._partials._nav_side_menu')

            </ul>
        </div>
    </div>
</nav>