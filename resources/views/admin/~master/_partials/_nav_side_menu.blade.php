<li>
    <a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
</li>

<li>
    <a href="{{ route('admin-persons') }}"><i class="fa fa-user fa-fw"></i> Users</a>
</li>

<li>
    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Memories<span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li>
            <a href="{{ route('admin-memories') }}">All</a>
        </li>
        <li>
            <a href="#">Option #1.2</a>
        </li>
    </ul>
</li>