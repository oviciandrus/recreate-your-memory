<!DOCTYPE html>
<html lang="en">
	<head>
		@include('admin.~master._partials._head')
	</head>

	<body>
		<div id="wrapper">
			@include('admin.~master._partials._nav')
			<div id="page-wrapper">
				<div class="row">
					@yield('page-title')
					@yield('page-wrapper')
				</div>
			</div>
		</div>
		@include('admin.~master._partials._scripts')
	</body>
</html>