<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="_url" content="{{ URL::to('/')}}"/>
    <meta name="_token" content="{{ csrf_token() }}"/>

    <title>Recreate Your Memory - Moderation Panel</title>
    
    {!! Html::style('vendors/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('vendors/metisMenu/metisMenu.min.css') !!}
    {!! Html::style('vendors/sb-admin/css/sb-admin-2.css') !!}
    {!! Html::style('vendors/font-awesome/css/font-awesome.min.css') !!}
    {!! Html::style('vendors/formvalidation/dist/css/formValidation.min.css') !!}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form id="login-form" role="form" method="post" action="{{ route('login-attempt') }}">
                        	{{ csrf_field() }}
                            <fieldset>
                                
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" id="email" name="email" type="text" autofocus readonly>
                                </div>
                                
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" id="password" type="password" readonly>
                                </div>


                                <button type="submit" class="btn btn-lg btn-success btn-block">Login</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Html::script('vendors/jquery/jquery.js') !!}
    {!! Html::script('vendors/bootstrap/js/bootstrap.min.js') !!}
    {!! Html::script('vendors/formvalidation/dist/js/formValidation.min.js') !!}
    {!! Html::script('vendors/formvalidation/dist/js/framework/bootstrap.min.js') !!}
    {!! Html::script('vendors/metisMenu/metisMenu.min.js') !!}
    {!! Html::script('vendors/sb-admin/js/sb-admin-2.js') !!}
    {!! Html::script('admin/js/comptech-soft.js') !!}
    {!! Html::script('admin/js/login/index.js') !!}
</body>
</html>