@extends('admin.~master.index')

@section('page-styles')
	{!! Html::style('vendors/jquery.dataTables/jquery.dataTables.min.css') !!}
	{!! Html::style('vendors/jquery.dataTables/dataTables.responsive.css') !!}
@stop

@section('page-title')
	<div class="col-lg-12">
		<h1 class="page-header">Users</h1>
	</div>
@stop

@section('page-wrapper')
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			@if(0)
				<div class="panel-heading">
					DataTables Advanced Tables
				</div>
			@endif

			<div class="panel-body">
				<table width="100%" class="table table-striped table-bordered table-hover" id="memories-table">
					<thead>
						<tr>
							<th>Id</th>
                			<th>Email</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
@stop


@section('page-scripts')
	{!! Html::script('vendors/jquery.dataTables/jquery.dataTables.min.js') !!}
	{!! Html::script('vendors/jquery.dataTables/dataTables.responsive.js') !!}
	<script>
	$(document).ready( function(){


		$('#memories-table').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: {type:'post', url: '{!! route("admin-persons-data") !!}'},
	        columns: [
	            { data: 'id', name: 'id' },
	            { data: 'email', name: 'email', orderable : false }
	        ]
	    });

	});
	</script>
@stop