@extends('~layouts.master')

@section('styles')
	{!! Html::style('vendors/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') !!}
	{!! Html::style('vendors/formvalidation/dist/css/formValidation.min.css') !!}
	{!! Html::style('vendors/ion.checkRadio/css/ion.checkRadio.css') !!}
	{!! Html::style('vendors/ion.checkRadio/css/ion.checkRadio.green.css') !!}
	{!! Html::style('css/create.css') !!}
@stop

@section('scripts')
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2X74xWumQ1zt5WgBagWAszEpYJqC1nEU&amp;libraries=places"></script>
	{!! Html::script('vendors/geocomplete/jquery.geocomplete.js') !!}
	{!! Html::script('vendors/moment/moment.js') !!}
	{!! Html::script('vendors/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}
	{!! Html::script('vendors/dropzone/dropzone.js') !!}
	{!! Html::script('vendors/formvalidation/dist/js/formValidation.min.js') !!}
	{!! Html::script('vendors/formvalidation/dist/js/framework/bootstrap.min.js') !!}
	{!! Html::script('vendors/ion.checkRadio/js/ion.checkRadio.min.js') !!}
	{!! Html::script('vendors/scroll-to/jquery-scrollto.js') !!}
	{!! Html::script('vendors/jquery.nicescroll/jquery.nicescroll.min.js') !!}
	<script>
		var messages = {!! json_encode(trans('create.validations')); !!};
		@if($person)
			var preview = {!! json_encode($preview) !!};
		@else
			var preview = null;
		@endif
	</script>
	{!! Html::script('js/create/step-1.js') !!}
	{!! Html::script('js/create/step-2.js') !!}
	{!! Html::script('js/create/init.js') !!}
@stop

@section('content')
<div id="create-container">

	<div class="go-to-page" data-page="1" id="go-to-page-1"></div>
	<div class="go-to-page" data-page="2" id="go-to-page-2"></div>
	<div class="go-to-page" data-page="3" id="go-to-page-3"></div>

	@include('create._steps._1')
	@include('create._steps._2')
	@include('create._steps._3')
</div>
@stop