<div class="create-step" id="create-step-2"  data-page="2">

	<h1>{{ trans('create.step-2.title') }}</h1>

	@if(0)
	<div class="error-exists" id="error-exists-2">
		<div class="error-title">{{ trans('create.error-title') }}</div>
		<div class="error-message">{{ trans('create.error-message') }}</div>
	</div>
	@endif

	<div id="memory-photo-upload">
		<form id="upload-memory-photo-form" method="post" action="{{route('upload-memory-photo')}}">
			{{ csrf_field() }}
			<input type="hidden" name="person_id" value="{{ $person ? $person->id : ''}}" id="person_id" />
		</form>	

		<div id="memory-photo-upload-message"></div>
		<div id="memory-photo-upload-progress" class="progress">
			<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
			</div>
		</div>
	</div>
	<h2 id="memory-photo-upload-info">{!! trans('create.step-2.memory-photo-upload') !!}</h2>

	<div id="memory-info">
		{!! trans('create.step-2.info') !!}
	</div>

	<div id="memory-informations-form">

		<form id="step-2-form" name="memory-details" class="form-validation" method="post">
			<input type="hidden" name="memory_id" value="" id="memory_id" />
			<!-- Name -->
			<div class="form-group">
				<input type="text" class="form-control form-memory" id="txtMemoryName" name="txtMemoryName" placeholder="{{ trans('create.step-2.name') }}" maxlength="250" />
			</div>
		
			<!-- memory description -->
			<div class="form-group form-group-description">
				<label id="lblDescription">{{ trans('create.step-2.description') }}</label>
				<textarea class="form-control form-memory" id="txtDescription" name="txtDescription" maxlength="1000"></textarea>
			</div>

			<!-- Location -->
			<div class="form-group" id="form-group-location">
				<input type="text" class="form-control form-memory" id="txtLocation" name="txtLocation" placeholder="{{ trans('create.step-2.location') }}">

			</div>

			<div id="map-preview"></div>
			<div id="map-details">
				<div class="form-group">
					<label class="label">{{trans('create.step-2.latitude') }}</label>
					<input readonly="readonly" type="text" data-geo="lat" class="form-control form-memory" id="txtLatitude" name="txtLatitude" />
				</div>
				<div class="form-group">
					<label class="label">{{trans('create.step-2.longitude') }}</label>
					<input readonly="readonly" type="text" data-geo="lng" class="form-control form-memory" id="txtLongitude" name="txtLongitude" />
				</div>
				<div class="form-group">
					<label class="label">{{trans('create.step-2.country-code') }}</label>
					<input readonly="readonly" type="text" data-geo="country_short" class="form-control form-memory" id="txtCountryCode" name="txtCountryCode" />
				</div>
				<div class="form-group">
					<label class="label">{{trans('create.step-2.country') }}</label>
					<input readonly="readonly" type="text" data-geo="country" class="form-control form-memory" id="txtCountry" name="txtCountry" />
				</div>
			</div>

			<!-- date -->
			<div id="form-group-date" class="form-group">
				<div class='input-group date' id='datetimepicker1'>
                    <input readonly="readonly" type='text' class="form-control" placeholder="{{ trans('create.step-2.date') }} " id="txtDate" name="txtDate"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
			</div>

			<div class="clearfix"></div>
			<!-- Tags -->
			<div class="form-group">
				<input type="text" class="form-control form-memory" id="txtTags" name="txtTags" placeholder="{{ trans('create.step-2.tags') }}">
			</div>
		</form>
	</div>

	<div id="terms-newsletter-options">
		<label>
			<input type="checkbox" name="terms-options" value="terms"/> 
			{!! trans('create.step-2.option-terms') !!}
		</label>
		<label>
			<input type="checkbox" name="newsletter-options" value="newsletter" /> 
			{!! trans('create.step-2.option-newsletter') !!}
		</label>
	</div>

	<div class="step-navigation" id="step-2-navigation">
		<div id="finish-message-memory" class="finish-message"></div>
		<button class="btn-no bk-full btn-rym btn-rym-blue" id="btn-step-2-next">
			<span class="caption">{{ trans('create.next') }}</span>
			<span class="indicator indicator-white"></span>
		</button>
		<button class="btn-no bk-full btn-rym btn-rym-bordered" id="btn-step-2-back">{{ trans('create.back') }}</button>
		<div class="clearfix"></div>
	</div>

</div>