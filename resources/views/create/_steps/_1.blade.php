<div class="create-step" id="create-step-1" data-page="1">

	<h1>{{ trans('create.step-1.title') }}</h1>

	<div id="sociality-login">

		<h2>{!! trans('create.step-1.info') !!}</h2>

		<button class="btn-no bk-full btn-rym btn-sociality-login" id="btn-sociality-facebook" data-url="{{ route('login-with-facebook') }}">
			{{ trans('create.step-1.login-with-facebook') }}
		</button>

		<button class="btn-no bk-full btn-rym btn-sociality-login" id="btn-sociality-twitter" data-url="{{ route('login-with-twitter') }}">
			<span class="caption">{{ trans('create.step-1.login-with-twitter') }}</span>
		</button>
		
		<div class="clearfix"></div>
	</div>

	@if(0)
	<div class="error-exists" id="error-exists-1">
		<div class="error-title">{{ trans('create.error-title') }}</div>
		<div class="error-message">{{ trans('create.error-message') }}</div>
	</div>
	@endif
	
	<!-- User photo -->
	<div id="user-photo-form">
		<div id="user-photo-upload">
			<form id="upload-person-photo-form" method="post" action="{{route('upload-person-photo')}}">
				{{ csrf_field() }}
			</form>
		</div>
		<h2 id="user-photo-upload-info">{!! trans('create.step-1.user-photo-upload') !!}</h2>
		<div id="user-photo-upload-message">Error</div>
		<div id="user-photo-upload-progress" class="progress">
			<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
			</div>
		</div>
	</div>

	<!-- User form -->
	<div id="user-informations-form">
		<form id="step-1-form" name="person-details" class="form-validation" method="post">

			<input type="hidden" name="socialite-user-id" id="socialite-user-id" value="{{$person ? $person->id : null}}" />

			<!-- First name -->
			<div class="form-group">
				<input type="text" class="form-control form-user" id="txtFirstName" name="txtFirstName" placeholder="{{ trans('create.step-1.first-name') }}" value="{{ $person ? $person->first_name : NULL }}">
			</div>
		
			<!-- Last name -->
			<div class="form-group">
				<input type="text" class="form-control form-user" id="txtLastName" name="txtLastName" placeholder="{{ trans('create.step-1.last-name') }}" value="{{ $person ? $person->last_name : NULL }}">
			</div>

			<!-- Email -->
			<div class="form-group">
				<input type="text" class="form-control form-user" id="txtEmail" name="txtEmail" placeholder="{{ trans('create.step-1.email') }}" value="{{ $person ? $person->email : NULL }}">
			</div>

			<!-- Nickname -->
			<div class="form-group">
				<input type="text" class="form-control form-user" id="txtNickname" name="txtNickname" placeholder="{{ trans('create.step-1.nickname') }}" value="{{ $person ? $person->nickname : NULL }}">
			</div>
		</form>
	</div>

	<div class="clearfix"></div>

	<div class="step-navigation" id="step-1-navigation">
		<button class="btn-no bk-full btn-rym btn-rym-blue" id="btn-step-1-next">
			<span class="caption">{{ trans('create.next') }}</span>
			<span class="indicator indicator-white"></span>
		</button>
		<button class="btn-no bk-full btn-rym btn-rym-bordered" id="btn-step-1-back">{{ trans('create.back') }}</button>
		<div class="clearfix"></div>
	</div>
</div>