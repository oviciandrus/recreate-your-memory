<div class="create-step" id="create-step-3"  data-page="3">


	<h1>{{ trans('create.step-3.title') }}</h1>

	<div id="thank-you-info">
		{{ trans('create.step-3.thank-you-info') }}
	</div>

	<div id="notification-options">
		<h2>{!! trans('create.step-2.notifications-info') !!}</h2>
		<label>
			<input type="checkbox" name="notification-options" value="1" /> 
			{!! trans('create.step-2.option-same-country') !!}
		</label>
		<label>
			<input type="checkbox" name="notification-options" value="2" /> 
			{!! trans('create.step-2.option-same-year') !!}
		</label>
		<label>
			<input type="checkbox" name="notification-options" value="3" /> 
			{!! trans('create.step-2.option-comments') !!}
		</label>
		<label>
			<input type="checkbox" name="notification-options" value="4" /> 
			{!! trans('create.step-2.option-same-tags') !!}
		</label>
		<div class="clearfix"></div>
	</div>

	<div class="step-navigation" id="step-3-navigation">
		<div id="save-message-memory" class="finish-message"></div>
		<button data-url="{{route('neuronal-view')}}" class="btn-no bk-full btn-rym btn-rym-blue" id="btn-step-3-view">
			<span class="caption">{{ trans('create.step-3.view') }}</span>
			<span class="indicator indicator-white"></span>
		</button>
	</div>
</div>