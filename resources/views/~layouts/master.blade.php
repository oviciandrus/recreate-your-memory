<!DOCTYPE html>
<html lang="en">
    
    @include('~layouts._partials._head')
    
    <body>

        <div id="wrapper">
            @include('~layouts._partials._nav')
            @yield('content')
        </div>

        @include('~layouts._partials._burger')

        {!! Html::script('vendors/jquery/jquery.js') !!}
        {!! Html::script('vendors/jquery/jquery-ui.min.js') !!}
        {!! Html::script('vendors/jquery/jquery.backstretch.min.js') !!}
        {!! Html::script('vendors/bootstrap/js/bootstrap.min.js') !!}
        {!! Html::script('vendors/bootstrap/js/ie10-viewport-bug-workaround.js') !!}
        {!! Html::script('vendors/bootstrap-select/js/bootstrap-select.min.js') !!}
        {!! Html::script('js/rym/rym.js') !!}

        @yield('scripts')
    </body>
</html>