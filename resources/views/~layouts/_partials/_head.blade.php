<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <meta name="csrftoken" content="{{ csrf_token() }}"/>
    <meta name="baseurl" content="{{ URL::to('/') }}"/>

    <link rel="icon" href="{{ URL::to('favicon.ico') }}">

    <title>Recreate Your Memory</title>

    {!! Html::style('vendors/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('vendors/bootstrap/css/ie10-viewport-bug-workaround.css') !!}
    {!! Html::style('vendors/bootstrap-select/css/bootstrap-select.min.css') !!}
    {!! Html::style('vendors/loaders/loaders.min.css') !!}
    {!! Html::style('css/app.css') !!}

    <link rel="apple-touch-icon" href="/bootstrap/img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/bootstrap/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/bootstrap/img/apple-touch-icon-114x114.png">

    {!! Html::script('vendors/modernizr/modernizr.js') !!}

    @yield('styles')

<style>

    </style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>