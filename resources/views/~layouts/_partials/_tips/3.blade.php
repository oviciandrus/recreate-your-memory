<h1>{{ trans('intro.tips.5.title') }}</h1>

<p>{{ trans('intro.tips.5.info') }}</p>

<div id="get-started">
	<button id="btn-get-started" class="btn-no bk-full btn-rym btn-rym-bordered btn-filter">
	    {!! trans('intro.tips.5.btn') !!}
	</button>
</div>