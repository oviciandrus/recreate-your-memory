<div class="modal fade" id="search-panel-modal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
           
            <div class="modal-body">

                <div id="search-by-tags">
                    <span>{!! trans('network-view.filter.search') !!}</span>
                    <div class="form-group">
                        <input type="text" class="form-control" id="input-tags" name="input-tags" placeholder="{{ trans('network-view.filter.search-placeholder') }}">
                    </div>
                </div>

                <div id="search-controls">
                    
                    <div id="search-by-years">
                        <span>{!! trans('network-view.filter.year') !!}</span>
                        <div>
                            <span id="year-from">{{$year_from}}</span> - <span id="year-to">{{$year_to}}</span>
                        </div>
                        <div class="form-group" id="years-form-group">
                            <div class="input-group">
                                <input type="text" id="years-range" class="form-control" name="years" value="" />
                            </div>
                        </div>
                    </div>

                    <div id="select-by-continent" class="select-container">

                        <span>{!! trans('network-view.filter.continent') !!}</span>
                        <div class="form-group">
                            <select id="continent_id" class="selectpicker form-control" data-live-search="true" data-header="{!! trans('network-view.filter.select-continent') !!}">
                                @foreach(App\Models\Memories\VContinentMemoriesCount::get() as $i => $continent)
                                    <option value="{{$continent->id ? $continent->id : 0}}">
                                        {{ $continent->continent_name ? $continent->continent_name : 'Without continent'}} ({{ $continent->memories_count }})
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div  id="select-by-country" class="select-container">
                        <span>{!! trans('network-view.filter.country') !!}</span>
                        <span id="filter-loading-indicator" class="pull-right"></span>
                        <div class="form-group">
                            <select id="country_id" class="selectpicker form-control" data-live-search="true" data-header="{!! trans('network-view.filter.select-country') !!}">
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                
                    <div id="error-messages">{{ trans('network-view.filter.error') }}</div>

                    
                </div>

                <div id="search-buttons-group">
                        <button id="btn-filter-apply" class="btn-no bk-full btn-rym btn-rym-bordered btn-filter">
                            {!! trans('network-view.filter.apply') !!}
                        </button>

                        <button id="btn-filter-reset" class="btn-no bk-full btn-rym btn-rym-bordered btn-filter">
                            {!! trans('network-view.filter.reset') !!}
                        </button>

                        <button id="btn-filter-cancel" class="btn-no bk-full btn-rym btn-rym-bordered btn-filter">
                            {!! trans('network-view.filter.cancel') !!}
                        </button>
                    </div>
            </div>
        </div>
    </div>
</div>