<div class="modal modal-transparent fade" id="slide-intro-modal" tabindex="-1">
    <div class="modal-dialog modal-dialog-transparent" role="document">   
        <div class="modal-content">
            <div class="modal-body">
            </div>
        </div>
    </div>

    <div class="modal-dialog modal-dialog-real" role="document">   
        <div class="modal-content">
           
            <div class="modal-body">

                <h1>{{ trans('intro.welcome') }}</h1>

                <p>
                    {{ trans('intro.intro') }}
                </p>

                <button id="btn-learn-more" class="btn-no bk-full btn-rym btn-rym-blue">
                    {{ trans('intro.learn-more') }}
                </button>

                <div id="skip">
                    <a href="javascript:;">{{ trans('intro.skip') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>