<div id="navigation">

	<div id="logo">
		<a class="bk-full" href="{{ route('neuronal-view') }}"></a>
	</div>


	<div id="burger">
		<button id="burger-navigation" class="btn-no bk-full"></button>
	</div>

	<div id="ajax-loader">
		<div data-loader="circle-side"></div>
	</div>

	<div class="clearfix"></div>

</div>