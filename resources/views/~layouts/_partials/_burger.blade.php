<div class="modal fade" id="burger-menu-modal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div id="menu-controls">

                    <div id="select-by-language" class="select-container-white">
                        <span>{!! trans('burger.language') !!}</span>
                        <div class="form-group">
                            <select id="language_id" class="selectpicker show-tick form-control">
                                @foreach( trans('burger.languages') as $key => $language)
                                    <option class="flag flag-{{$key}}" value="{{$key}}" {{$key == App::getLocale() ? 'selected="selected"' : ''}}>
                                        {{$language}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div id="options">
                        <div id="option-create" class="menu-options-item">
                            <a href="{{ route('create') }}">{{ trans('burger.options.create') }}</a>
                        </div>
                        <div id="option-neuronal-view" class="menu-options-item">
                            <a href="{{ route('neuronal-view') }}">{{ trans('burger.options.neuronal-view') }}</a>
                        </div>
                        <div id="option-map-view" class="menu-options-item">
                            <a href="{{ route('map-view') }}">{{ trans('burger.options.map-view') }}</a>
                        </div>
                        <div id="option-about" class="menu-options-item">
                            <a href="#">{{ trans('burger.options.about') }}</a>
                        </div>
                        <div id="option-win" class="menu-options-item">
                            <a href="#">{{ trans('burger.options.win') }}</a>
                        </div>
                    </div>

                    <div id="links">
                        <div class="link-item"><a href="#">{{ trans('burger.options.terms') }}</a></div>
                        <div class="link-item"><a href="#">{{ trans('burger.options.imprint') }}</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>