<div class="modal modal-transparent fade" id="slide-tips-modal" tabindex="-1">
    <div class="modal-dialog modal-dialog-transparent" role="document">   
        <div class="modal-content">
            <div class="modal-body">
            </div>
        </div>
    </div>
    <div class="modal-dialog" role="document">
        <div class="modal-content">           
            <div class="modal-body">
                <button id="btn-close" class="btn-no bk-full btn-rym"></button>
                <div id="tips-container">
                    <button id="btn-left" class="btn-no btn-arrows"></button>
                    <button id="btn-right" class="btn-no btn-arrows"></button>
                    @foreach([0, 1, 2, 3] as $i => $page)
                        <div id="tips-page-{{$page}}" class="tips-page">
                            @include('~layouts._partials._tips.' . $page)
                        </div>
                    @endforeach
                    <div id="tips-pagination">
                        @foreach([0, 1, 2, 3] as $i => $page)
                            <button id="btn-go-to-page-{{$page}}" class="btn-no bk-full btn-rym btn-tip-item {{$page == 2 ? 'btn-tip-item-selected' : 'btn-tip-item-unselected'}}" data-page="{{$page}}"></button>
                        @endforeach
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>