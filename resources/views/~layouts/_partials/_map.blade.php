<div id="map-view">
	<div id="map-view-overlay">
	</div>
</div>
<div id="memories-count">
	<strong id="memories-count-value">0</strong> / 
	<strong>{{App\Models\Memories\VApprovedMemory::count()}}</strong> 
	{{ trans('map-view.memories-found') }}
</div>

<div id="map-view-controls">
	<button id="btn-plus" class="btn-no bk-full btn-default-size" title="{{ trans('network-view.add-memory-tooltip') }}"></button>
	<button id="btn-search" class="btn-no bk-full btn-default-size"></button>
</div>

<div id="memory-tooltip-transparent" class="tooltip-transparent"></div>
<div id="memory-tooltip" data-url="">

	<div id="memory-tooltip-inner">
		<div id="memory-tooltip-left">
			<div id="user-avatar">
				<img class="img-circle" src="" alt="memory-photo" />
			</div>
		</div>

		<div id="memory-tooltip-right">
			
			<!-- small -->
			<div id="memory-name" class="no-wrap view-small">
			</div>
			<div id="memory-location" class="no-wrap view-small">
				<span id="m-location" class="view-small"></span>,
				<span id="m-date-of" class="font-light view-small"></span>
			</div>
			<div id="memory-likes" class="view-small">
				<span id="m-likes" class="view-small"></span>
				<span id="m-comments" class="view-small"></span>
			</div>


			<!-- large -->
			<div id="memory-name" class="no-wrap view-large">
			</div>

			<div id="memory-location" class="no-wrap view-large">
				<span id="m-location" class="view-large"></span>,
				<span id="m-date-of" class="font-light view-large"></span>
				<span id="m-likes" class="view-large"></span>
				<span id="m-comments" class="view-large"></span>
			</div>

		</div>

		<div class="clearfix"></div>
	</div>
</div>

@include('~layouts._partials._search')
@include('~layouts._partials._slide-intro')
@include('~layouts._partials._slide-tips')