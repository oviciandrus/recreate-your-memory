<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        @include('emails._partials._head')
    </head>

    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", Georgia, Times, sans-serif">

        <!-- Wrapper -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td width="100%" valign="top" bgcolor="#ffffff" style="padding-top:20px">
                    @include('emails._partials._header')
                    @include('emails._partials._one-column')
                    @include('emails._partials._footer')
                </td>
            </tr>
        </table> <!-- End Wrapper -->
        <div style="display:none; white-space:nowrap; font:15px courier; color:#ffffff;">
            - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        </div>
    </body>
</html>