<!-- One Column -->
<table width="580" class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#fff" style="margin:0 auto;">
	
    <tr>
		<td valign="top" style="padding:60px 0px 45px 0px" bgcolor="#fff">

            <table width="580" class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#fff" style="margin:0 auto;">
                <tr>
                    <td style="width:25%;">&nbsp;</td>
                    <td style="width:50%; text-align:center; color:#000064">
                        <p style="mso-table-lspace:0;mso-table-rspace:0; font-size:28px; font-weight:bold; padding:0px; margin:5px 0px;">
                            {{ trans('emails.thank-you-email.thank-you', ['username' => $memory['nickname']]) }}
                        </p>
                        <p style="mso-table-lspace:0;mso-table-rspace:0; font-size:26px; padding:0px; margin:5px; 0px">
                            {{ trans('emails.thank-you-email.in-network') }}
                        </p>                        
                    </td>
                    <td style="width:25%">&nbsp;</td>
                </tr>
            </table>
		</td>
	</tr>

    <tr>
        <td valign="top" style="padding:0px 0px 45px 0px" bgcolor="#fff">

            <table width="580" class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#fff" style="margin:0 auto;">
                <tr>
                    <td style="width:25%;">&nbsp;</td>
                    <td align="center" style="width:50%; text-align:center; color:#000064">
                       
                        {!! Html::image($memory['email_photo'], 'memory-photo', ['title'=> 'Memory Photo', 'width' => '280', 'height' => '280', 'style' => 'display:block;width:280px; height:280px;']) !!}
                       
                        <p style="mso-table-lspace:0;mso-table-rspace:0; padding:0px; margin:20px 0px 0px 0px; text-align:left">
                            <a target="_blank" href="{{ route('detail-view', ['memory_id' => $memory['id'], 'slug_name' => $memory['slug_name'] ]) }}" style="font-size:18px; color:#000064; font-weight:bold;">{{ $memory['name'] }}</a>
                        </p>
                        
                        <p style="mso-table-lspace:0;mso-table-rspace:0; font-size:18px; color:#000064; margin:10px 0px 0px 0px; text-align:left">
                            {{ $memory['country_name'] }}, {{ $memory['year'] }}
                        </p>

                        <p style="mso-table-lspace:0;mso-table-rspace:0; width:263px; text-align:center; margin:40px auto 40px auto">
                            <a target="_blank" href="{{ route('detail-view', ['memory_id' => $memory['id'], 'slug_name' => $memory['slug_name'] ]) }}" style="display:block; width:263px; height:60px;">
                                {!! Html::image('assets/emails/view-my-memory-' . App::getLocale() . '.png', 'button-view', ['display:block; width:263px; height:60px', 'height' => '60', 'width' => '263', 'title' => 'Button View My Memory']) !!}
                            </a>
                        </p>

                        <p style="mso-table-lspace:0;mso-table-rspace:0; text-align:center; margin:0px; padding:0px;">
                            <a target="_blank" href="#" style="margin:0px; padding:0px; color:#000064; font-size:18px; font-weight:bold;">
                                {{ trans('emails.thank-you-email.change-settings') }}
                            </a>
                        </p>
                    </td>
                    <td style="width:25%">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>