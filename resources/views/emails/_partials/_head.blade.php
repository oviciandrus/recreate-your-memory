<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Recreate Your Memory</title>
<style type="text/css">
.ReadMsgBody 
{
    width: 100%; 
    background-color: #ffffff;
}
.ExternalClass
{
    width: 100%; background-color: #ffffff;
}
body
{
    width: 100%; 
    background-color: #ffffff; 
    margin:0; 
    padding:0; 
    -webkit-font-smoothing: antialiased;
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", Georgia, Times, sans-serif;
}
table 
{
    border-collapse: collapse;
}
@media only screen and (max-width: 640px)  
{
		body[yahoo] .deviceWidth 
        {
            width:440px!important; 
            padding:0;
        }
		body[yahoo] .center 
        {
            text-align: center!important;
        }
}
@media only screen and (max-width: 479px) 
{
		body[yahoo] .deviceWidth 
        {
            width:280px!important; padding:0;
        }
		body[yahoo] .center 
        {
            text-align: center!important;
        }
}
</style>