<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td bgcolor="#f8f8f8" style="padding:20px 20px">
            <table width="580" border="0" cellpadding="10" cellspacing="0" align="center" class="deviceWidth" style="margin:0 auto;">
                <tr>
                    <td width="100%" align="left" style="padding:0; margin:0; width:100%">
                        <table border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
                            <tr>
                                <td align="left" valign="top" style="font-size: 12px; color: #000064; padding-bottom:20px; text-align-left:important; margin:0;">
                                    Copyright &copy; 2016 {{ 2016 != Carbon\Carbon::now()->format('Y') ? ' - ' . Carbon\Carbon::now()->format('Y') : '' }} Lufthansa
                                </td>
                            </tr>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" align="right" class="deviceWidth">
                            <tr>
                                <td align="right" valign="top" style="font-size: 12px; color: #000064; padding-bottom:20px; text-align:right !important; margin:0;">
                                    <a style="display:inline-block; width:100%; color:#000064; font-size:12px; text-align:right !important" href="#">
                                        {{ trans('burger.options.imprint') }}
                                    </a>
                                </td>
                            </tr>
                        </table>
            		</td>
                </tr>

                <tr>
                    <td align="left" style="padding:0px;margin:0px">
                        <p style="color:#707070; mso-table-lspace:0;mso-table-rspace:0; font-size:12; padding:0px; margin:0px">
                            {{ trans('emails.thank-you-email.footer-info') }}
                        </p>
                    </td>

            	</tr>
            </table>
        </td>
    </tr>
</table>