<!-- Start Header-->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" style="margin:0 auto;">
    <tr>
        <td width="100%" bgcolor="#ffbe00">
            <!-- Logo -->
            <table border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
                <tr>
                    <td style="padding:10px 20px" class="center">
                        <a href="#">
                            {!! Html::image('assets/small/logo.png', 'logo', ['border' => 0, 'title' => 'logo', 'width' => '213', 'height' => '20']) !!}
                        </a>
                    </td>
                </tr>
            </table>
            <!-- End Logo -->

            <!-- Nav -->
            <table border="0" cellpadding="0" cellspacing="0" align="right" class="deviceWidth">
                <tr>
                    <td class="center" 
                        style="font-size: 12px; 
                               color: #000064; 
                               font-weight: normal; 
                               text-align: right; 
                               line-height: 20px; 
                               vertical-align: middle; 
                               padding:8px 20px;"
                    >
                        <a href="#" style="color: #000064; font-size:12px;">
                            {{ trans('emails.thank-you-email.view-in-browser') }}
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>