@extends('~layouts.master')

@section('styles')
	{!! Html::style('vendors/ion.rangeSlider/css/normalize.css') !!}
    {!! Html::style('vendors/ion.rangeSlider/css/ion.rangeSlider.css') !!}
    {!! Html::style('vendors/ion.rangeSlider/css/ion.rangeSlider.skinHTML5.css') !!}
    {!! Html::style('vendors/tooltipster/css/tooltipster.bundle.min.css') !!}  
	{!! Html::style('css/map.css') !!}
	{!! Html::style('css/map-view.css') !!}
@stop

@section('scripts')
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2X74xWumQ1zt5WgBagWAszEpYJqC1nEU"></script>
	<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

	{!! Html::script('vendors/ion.rangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js') !!}
    {!! Html::script('vendors/tooltipster/js/tooltipster.bundle.min.js') !!} 
	{!! Html::script('js/google-map/markerclusterer.js') !!}
	{!! Html::script('vendors/comptech/Collection.js') !!}
	{!! Html::script('js/google-map/mapview.js') !!}
	{!! Html::script('js/google-map/map-options.js') !!}
	{!! Html::script('js/google-map/jquery.mapview.js') !!}
	
	{!! Html::script('js/network-view/rym-neuronal-view.js') !!}
	{!! Html::script('js/map-view/rym-map-view.js') !!}
	{!! Html::script('js/map-view/run.js') !!}
@stop

@section('content')
	@include('~layouts._partials._map')
	<div id="google-map-view-overlay">
	</div>
@stop