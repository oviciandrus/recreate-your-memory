Rym.detail = {


	setBackground : function(photos)
	{
		Rym.utils.setBackground('#wrapper', photos.background, false);
		// Rym.utils.setBackground('#detail-section-1', photos.photo480, false);

		// $('#detail-section-1').css({
		// 	'height' : Rym.utils.width() + 'px'
		// });
	},

	setMap : function(Mapview, memory)
	{
		Mapview.options.center = {lat : parseFloat(memory.latitude), lng : parseFloat(memory.longitude)};
		Mapview.options.zoom = 3;
		Mapview.options.scrollwheel = false;
		Mapview.options.draggable = true;
		Mapview.options.disableDefaultUI = true;
	
		var myMap = $('.memory-small-map:visible').mapview(Mapview.options);

		myMap.mapview('addMarker', {
			lat : parseFloat(memory.latitude),
			lng : parseFloat(memory.longitude),
			icon : Rym.utils.baseUrl() + '/assets/small/marker-yellow.png',
			visible : true,
			id : 1,
			info_caption : memory.name,
			events : [
				{
					eventName : 'click',
					eventCallback : function(e, marker){ 
						// console.log('Click on ' + 1, marker);
					}
				},
			]
		});
	},

	validateCommentform : function(validations)
	{
		var fv = $(".comment-posting:visible").formValidation({
        	framework:'bootstrap',
	        icon:{
	        	valid : 'glyphicon glyphicon-ok', 
	        	invalid : 'glyphicon glyphicon-remove',
	        	validating :'glyphicon glyphicon-refresh'
	        },
	        fields: {
	    
	            txtFirstName: {
	                validators: {
	                    notEmpty: {message:validations.firstname.required},
	                }
	            },

	            txtLastName: {
	                validators: {
	                    notEmpty: {message:validations.lastname.required},
	                }
	            },

	            txtEmail: {
	                validators: {
	                    notEmpty: {message : validations.email.required},
	                    emailAddress : {message : validations.email.email}
	                }
	            },

	            txtNickname: {
	                validators: {
	                    notEmpty: {message:validations.nickname.required},
	                }
	            },

	            txtComment: {
	                validators: {
	                    notEmpty: {message:validations.comment.required},
	                }
	            },

	        }
	    }).on('success.form.fv', function(e) {
	    	// Prevent form submission
	    	e.preventDefault();
	        // 
	        var $form = $(e.target), fv = $form.data('formValidation');
	        var caption = $form.find('.caption').html();
	        $form.find('.caption').html('');
	        $form.find('.indicator').css({'display':'block'}).show();
	 		var data = {
	 			memory_id : $('#memory-id').val(),
	 			email : $form.find('#txtEmail').val(),
	 			first_name : $form.find('#txtFirstName').val(),
	 			last_name : $form.find('#txtLastName').val(),
	 			nickname : $form.find('#txtNickname').val(),
	 			comment : $form.find('#txtComment').val()
	 		};
	 		Rym.utils.fetchData('create-comment', data, 'post').then(function(response){
	            if(response.success)
	            {
	                $form.find('.indicator').css({'display':'block'}).hide();
	                $form.find('.caption').html(caption);
	                $form.find('#txtEmail').val('');
	                $form.find('#txtFirstName').val('');
	                $form.find('#txtLastName').val('');
	                $form.find('#txtNickname').val('');
	                $form.find('#txtComment').val('')
	            }
	 		}); 
	    });
	},

	onClickShowMoreComments : function()
	{
		$(document).on('click', '.btn-show-more-comments', function(e){
	        e.preventDefault();
	        var btn = $(this);
	        var next_url = btn.data('next-page-url');
	        if(next_url)
	        {
	            var caption = btn.find('.caption').html();
	            btn.find('.caption').html('');
	            btn.find('.indicator').css({'display':'block'}).show();
	            Rym.utils.fetchData(btn.data('next-page-url'), {}, 'get').then(function(response){
	                btn.find('.indicator').css({'display':'block'}).hide();
	                btn.find('.caption').html(caption);
	                $('.comment-item-container').removeClass('last-comment');
	                var id = btn.attr('id');
	                var content = response[ (id == 'btn-large-show-more-comments') ? 'page-large' : 'page-small'];
	                btn.before(content);
	                $c = $(content);
	                $('.comments-page[data-page="' + $c.data('page') + '"]').css({
	                    'opacity' : '0.25',
	                }).animate({
	                    'opacity' : '1'}
	                , 250).ScrollTo({
	                    duration: 1000,
	                    easing: 'linear'
	                });
	                btn.data('next-page-url', response['next-page-url']);
	                if(! response['next-page-url'])
	                {
	                    btn.hide();
	                }
	            });
	        }
	        else
	        {
	            btn.hide();
	        }
	        
	    })
	},

	onClickAddNewMemory : function()
	{
		$(document).on('click', '.btn-create-memory', function(e){
	        location.href = $('.btn-create-memory').data('url');
	    });
	},

	init : function()
	{
		this.onClickShowMoreComments();
		this.onClickAddNewMemory();
	}
}

Rym.detail.init();