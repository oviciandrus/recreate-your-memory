(function(window, $){
	
	var myMap = $('#map').mapview(Mapview.options);

	myMap.mapview('addMarker', {
		lat : 46.219702,
		lng : 24.796388,
		icon : 'http://localhost/rym-ct/public/images/markers/bank.png',
		draggable : true,
		visible : true,
		id : 1,
		info_caption : 'Caption ' + 1,
		events : [
			{
				eventName : 'click',
				eventCallback : function(e, marker){ 
					console.log('Click on ' + 1, marker);
				}
			},
			{
				eventName : 'dragend',
				eventCallback : function(e){ 
					console.log('Drag end on ' + 1);
				}
			}
		]
	});

	myMap.mapview('addMarker', {
		lat : 46.538586,
		lng : 24.55143,
		icon : 'http://localhost/rym-ct/public/images/markers/bank.png',
		draggable : true,
		visible : true,
		id : 2,
		info_caption : 'Caption ' + 2,
		events : [
			{
				eventName : 'click',
				eventCallback : function(e, marker){ 
					console.log('Click on ' + 2, marker);
				}
			},
			{
				eventName : 'dragend',
				eventCallback : function(e){ 
					console.log('Drag end on ' + 2);
				}
			}
		]
	});

	myMap.mapview('addMarker', {
		location : 'Sovata, RO',
		icon : 'http://localhost/rym-ct/public/images/markers/bank.png',
		draggable : true,
		visible : true,
		id : 3,
		info_caption : 'Caption ' + 3,
		events : [
			{
				eventName : 'click',
				eventCallback : function(e, marker){ 
					console.log('Click on ' + 3, marker);
				}
			},
			{
				eventName : 'dragend',
				eventCallback : function(e){ 
					console.log('Drag end on ' + 3);
				}
			}
		]
	});

	if(navigator.geolocation)
	{
		navigator.geolocation.getCurrentPosition( function(position){
			myMap.mapview('addMarker', {
				lat : position.coords.latitude,
				lng : position.coords.longitude,
				
				draggable : true,
				visible : true,
				id : 4,
				info_caption : 'Caption ' + 4,
				events : [
					{
						eventName : 'click',
						eventCallback : function(e, marker){ 
							console.log('Click on ' + 4, marker);
						}
					},
					{
						eventName : 'dragend',
						eventCallback : function(e){ 
							console.log('Drag end on ' + 4);
						}
					}
				]
			});
		});
	}
}(window, jQuery));