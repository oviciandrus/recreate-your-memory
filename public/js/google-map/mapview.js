(function(window, google, Collection){

	// acesta este un modul
	var Mapview = (function(){

		function Mapview(element, options)
		{
			this.gMap = new google.maps.Map(element, options);
			this.markers = Collection.create();
			if( options.clusterer )
			{
				this.clusterer = new MarkerClusterer(this.gMap, [], options.clusterer);
			}
			if( options.geocoder )
			{
				this.geocoder = new google.maps.Geocoder();
			}
			
		}

		Mapview.prototype = {
			zoom : function(zoomLevel)
			{
				if(zoomLevel)
				{
					this.gMap.setZoom(zoomLevel);
				}
				else
				{
					return this.gMap.getZoom();
				}
			},

			bindEvent : function( options )
			{
				// this.gMap
				var self = this;
				google.maps.event.addListener(options.targetObject, options.eventName, function(e){
					options.eventCallback.call(self, e, options.targetObject);
				});
			},

			removeMarker : function(marker)
			{
				if( this.markers.remove(marker) )
				{
					if( this.clusterer )
					{
						this.clusterer.removeMarker(marker);
					}
					else
					{
						marker.setMap(null);
					}
				}
			},

			removeAll : function()
			{
				this.clusterer.removeMarkers(this.markers.items);
				this.markers.removeAll();
			},

			createMarker : function(options)
			{
				var self = this;
				if( options.location )
				{
					this.geoCode({
						address : options.location,
						success : function(result){
							options.lat = result[0].geometry.location.lat(),
							options.lng = result[0].geometry.location.lng(),
							options.location = null;
							self.createMarker(options);
						},
						error : function(status){
						}
					});
				}
				else
				{
					options.position = {
						lat : options.lat,
						lng : options.lng
					};
					options.map = this.gMap;
					var marker = new google.maps.Marker(options);
					if( this.clusterer )
					{
						this.clusterer.addMarker(marker);
					}
					if( options.info_caption )
					{
						options.events.push({
							eventName : 'click',
							eventCallback : function(e)
							{
								// marker.info = new google.maps.InfoWindow({
								// 	content : options.info_caption,
								// });
								// marker.info.open(this.gMap, marker);
							}
						})
					}
					if(options.events)
					{
						for(var i = 0; i < options.events.length; i++)
						{
							this.bindEvent({
								targetObject : marker,
								eventName : options.events[i].eventName,
								eventCallback : options.events[i].eventCallback
							});
						}					
					}
					this.markers.add(marker);
					return marker;
				}
			},

			filterMarkers : function(callback)
			{
				return this.markers.find(callback);
			},

			setCenter : function(latitude, longitude)
			{
				this.gMap.setCenter({lat:latitude, lng:longitude});
			},
			
			setPanorama : function(element, options)
			{
				var panorama = new google.maps.StreetViewPanorama(element, options);
				this.gMap.setStreetView(panorama);
			},

			geoCode : function(options)
			{
				this.geocoder.geocode(
					{address : options.address}, 
					function(result, status){
						if(status === google.maps.GeocoderStatus.OK)
						{
							options.success.call(this, result);
						}
						else
						{
							options.error.call(this, status);
						}
					}
				);
			}
		};

		return Mapview;
	}());

	Mapview.create = function(element, options)
	{
		return new Mapview(element, options);
	}

	window.Mapview = Mapview;

}(window, google, Collection));