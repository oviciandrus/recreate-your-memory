(function(window, Mapview){

	$.widget("jquery.mapview", {

		// default options
		options : {},

		// the constructor
		_create : function()
		{
			this.map = Mapview.create(this.element[0], this.options);
		},

		getMap : function()
		{
			return this.map;
		},

		getClusterer : function()
		{
			return this.map.clusterer;
		},

		zoom : function(level)
		{
			return this.map.zoom(level);
		},

		// public methods
		addMarker : function(options)
		{
			return this.map.createMarker(options);
		},

		removeMarker : function(marker)
		{
			return this.map.removeMarker(marker);
		},

		removeAll : function()
		{
			return this.map.removeAll();
		},

		filterMarkers : function(callback)
		{
			return this.map.filterMarkers(callback);
		},

		getMarkers : function()
		{
			return this.map.markers.items;
		},

		setCenter : function(latitude, longitude)
		{
			this.map.setCenter(latitude, longitude);
		},

		setPanorama : function(element, options)
		{
			return this.map.setPanorama($(element)[0], options);
		},

		geoCode : function(options)
		{
			return this.map.geoCode(options);
		}
	});

}(window, Mapview));