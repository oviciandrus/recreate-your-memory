(function(window, google, Mapview){

	var url = $('meta[name="baseurl"]').attr('content');
	// 
	Mapview.options = {
		center : {
			lat : 46.219702,
			lng : 24.796388
		},
		zoom : 1,
		disableDefaultUI : false,
		scrollwheel : true,
		draggable : true,
		mapTypeId : google.maps.MapTypeId.ROADMAP, // SATELLITE, //HYBRID
		maxZoom : 21,
		minZoom : 1,

		// mapTypeControl: false,
		mapTypeControlOptions: 
		{
		    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
		    mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.HYBRID],
		    position: google.maps.ControlPosition.LEFT_TOP,
		},

		zoomControl: true,
		zoomControlOptions : {
			position: google.maps.ControlPosition.LEFT_CENTER,
			style : google.maps.ZoomControlStyle.SMALL
		},
		streetViewControl: false,
		scaleControl : false,
  		rotateControl: false,
  		fullscreenControl: false,
		// panControlOptions : {
			// position: google.maps.ControlPosition.TOP_RIGHT,
		// },

		// panControlOptions="{position: google.maps.ControlPosition.RIGHT_BOTTOM}"
// scaleControlOptions ="{position: google.maps.ControlPosition.RIGHT_TOP}"
		
		

		clusterer : { 
			gridSize : 80,
			maxZoom : 14,
			'styles' : [
				{
					url: url + '/assets/map-view/c2.png',
					height: 50,
					width: 50,
					// anchor: [16, 0],
					textColor: '#ffffff',
					textSize: 10,
					fontWeight : 'bold'
				}, 
				{
					url: url + '/assets/map-view/c3.png',
					height: 75,
					width: 75,
					// anchor: [16, 0],
					textColor: '#ffffff',
					textSize: 12,
					fontWeight : 'bold'
				}, 
				{
					url: url + '/assets/map-view/c4.png',
					height: 100,
					width: 100,
					// anchor: [24, 0],
					textColor: '#ffffff',
					textSize: 14,
					fontWeight : 'bold'
				},
				{
					url: url + '/assets/map-view/c5.png',
					height: 125,
					width: 125,
					// anchor: [32, 0],
					textColor: '#ffffff',
					textSize: 16,
					fontWeight : 'bold'
				}
			]
		},
		geocoder : true,

		styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
		
	};

}(window, google, window.Mapview));