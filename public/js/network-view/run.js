(function(window, $, sigma){

	$(document).ready(function(){

		Rym.neuronal.init();
		Rym.neuronal.onFilterApply('network-view');

		window.MyNetwork = Network.create({
            maxNumberOfNodes : 4761, // numarul maxim de noduri la un moment dat
        });
        
		MyNetwork.calculatePageLength(Rym.utils.width(), Rym.utils.height());

		var 
			maxx = (MyNetwork.pageWidth - 1)/2 * MyNetwork.step.x, 
			maxy = (MyNetwork.pageHeight - 1)/2 * MyNetwork.step.y;

		MyNetwork.loadPages([
			{
				page : 1,
				rectangle : {
					PMin : {x : -maxx, y : -maxy},
				}
			},
		]);

	});

}(window, $, sigma));

/*

$(function() {
    var delay = 3,
        span = $('span'),
        posts = [
            {
                input1: 'My name 1',
                input2: 'My address 1',
                input3: 'My country 1'
            },
            {
                input1: 'My name 2',
                input2: 'My address 2',
                input3: 'My country 2'
            },
            {
                input1: 'My name 3',
                input2: 'My address 3',
                input3: 'My country 3'
            },
            {
                input1: 'My name 4',
                input2: 'My address 4',
                input3: 'My country 4'
            }
        ],
        looper = $.Deferred().resolve();

    $.each(posts, function(i, data) {
        looper = looper.then(function() {
            return $.ajax({
                data: {
                    json: JSON.stringify(data),
                    delay: delay,
                    _token : 'hEtVThLlB2KUEqzkYpP1RJqAAMqwA2cRiFMiBR4f',
                },
                method: 'post',
                url: 'http://localhost/rym-ct/public/echo/json',
                dataType: 'json'
            }).done(function(response) {
                // span.append('Response:<br />');
                console.log(response);
                console.log('Waiting ' + delay + ' seconds<br /><br />');
            });
        });
    });
});

*/