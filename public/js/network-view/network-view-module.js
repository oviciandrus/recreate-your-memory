(function(w, $, sigma){

	var Network = (function(){

		function Network(options)
		{
			sigma.classes.graph.addMethod('getNodesIds', function(){return this.nodesIndex;});
			// this.click = {};
			this.maxNumberOfNodes = options.maxNumberOfNodes;
			this.filterOptions = null;
			this.click = {};
			this.step = { x : 170, y : 150,};
			this.map = {PMin : {x : 0, y : 0}, PMax : {x : 0, y : 0},};
			
			this.pageLength = null;
			this.pageWidth = null;
			this.pageHeight = null;
			
			this.currentPage = 1;
			this.lastPage = 100000;
			this.pageUrl = 'neuronal-view-get-page';
			
			this.noMoreNodes = false;
			
			// ce nod se incarca
			this.loadingNode = null;
			// ce pagin s-au icarcat
			this.loadedPages = [];

			// cum sa se afiseze nodurile incarcate
			// initial sa fie luminoase
			this.opacity = false;
			this.main = null;
		
			// this.cells = {};
			this.s = new sigma({
	            renderer: {container: document.getElementById('map-view'), type: 'canvas'},
	            settings: {
	                autoRescale        : false,
	                enableCamera       : true,
	                rescaleIgnoreSize  : false,
	                mouseWheelEnabled  : true,
	                doubleClickEnabled : true,
	                skipErrors         : false,
	                verbose            : true,
	                enableHovering     : true,
	                enableEdgeHovering : false,
	                batchEdgesDrawing  : false,
	                hideEdgesOnMove    : false,
	                singleHover        : true,
	                borderSize         : 3,
	            }
	        });
		}

		Network.prototype = {

			getNodes : function(){ return this.s.graph.nodes(); },
			getNodeByIndex : function(i){ return this.getNodes()[i];},
			getNodeById : function(id){return this.s.graph.nodes(id);},
			getLoadedIds : function(){ return this.s.graph.getNodesIds().keyList();},
			nodesCount : function(){ return this.s.graph.nodes().length;},
			getEdges : function(){ return this.s.graph.edges(); },
			edgesCount : function(){ return this.s.graph.edges().length;},
			
			mapEmpty : function()
			{
				return this.nodesCount() == 0;
			},

			selectNode : function(node)
			{
				if( ! node.selected )
				{
					node.selected = true;
					node.opacity = false;
				}
			},

			refresh : function()
			{
				this.s.refresh();
				$('#count-nodes').html( this.nodesCount() );
			},

			clear : function()
			{
				this.s.graph.clear();
				this.s.refresh();
			},

			drawNode : function(node, x, y, selected, opacity)
			{
				var self = this;
				this.loadingNode = node.id;
        		sigma.canvas.nodes.image.cache(node.photo, function(){
        			self.s.graph.addNode({
        				id       : node.id,
        				type     : 'image',
        				url      : node.photo,
        				x        : x,
        				y        : y,
        				size     : node.view_relevance,
        				selected : selected, // ca sa aiba sau nu cerc inconjurator,
        				opacity  : opacity,  // deca este opac sau nu
        				hovered  : false,

        				photo : node.photo,
        				name : node.name,
        				country_name : node.country_name,
        				_date_of : node._date_of,
        				detail_url : node.detail_url,
        				count_likes : node.count_likes,
        				count_comments : node.count_comments,
        			});
        			self.refresh();
        			Rym.neuronal.updateMemoryLoadedCount(self.nodesCount());
        			self.loadingNode = null;
        		});
    		},

    		putEdge : function(edge, selectNodes, refresh)
    		{
    			if(selectNodes)
    			{
    				this.selectNode(this.getNodeById(edge.source_id));
    				this.selectNode(this.getNodeById(edge.target_id));
    			}
    			this.s.graph.addEdge({
    				id : edge.id,
    				color : edge.color,
    				source : edge.source_id,
    				target : edge.target_id,
    				type : 't',
    			});
    			if(refresh)
    			{
    				this.refresh();
    			}
    		},

    		unselectNodes : function()
    		{
    			var nodes = this.s.graph.nodes();
    			for(var i = 0; i < nodes.length; i++)
    			{
    				nodes[i].selected = false;
    				nodes[i].opacity = true;
    			}
    		},

    		selectNodes : function()
    		{
    			var nodes = this.s.graph.nodes();
    			for(var i = 0; i < nodes.length; i++)
    			{
    				nodes[i].selected = true;
    				nodes[i].opacity = false;
    			}
    		},

    		putEdges : function(edges)
    		{
    			for(var i = 0; i < edges.length; i++)
    			{
    				this.putEdge(edges[i], true, false);
    			}
    		},

    		removeEdges : function()
    		{
    			var edges = this.s.graph.edges();
    			for(var i = 0; i < edges.length; i++)
    			{
    				this.s.graph.dropEdge(edges[i].id);
    			}
    		},

    		calculatePageLength : function(width, height)
    		{
    			var cntHorizontalNodes = Math.ceil(width / this.step.x + 1);
    			var cntVerticalNodes = Math.ceil(height/ this.step.y + 1);
    			if(cntHorizontalNodes % 2 == 0)
				{
					cntHorizontalNodes++;
				}
				if(cntVerticalNodes % 2 == 0)
				{
					cntVerticalNodes++;
				}
    			this.pageWidth = Math.max(cntHorizontalNodes, cntVerticalNodes);
    			this.pageHeight = this.pageWidth;
    			this.pageLength = this.pageWidth * this.pageHeight;
    		},

    		putMemories : function(nodes, rectangle)
    		{
    			if(nodes.length == 0)
    			{
    				this.noMoreNodes = true;
    				return;
    			}
    			rectangle.PMax = {
					x : rectangle.PMin.x + this.step.x * (this.pageWidth - 1),
					y : rectangle.PMin.y + this.step.y * (this.pageHeight - 1)
				};
  
  				console.log('PUT MEMORIES', rectangle);
    			var x1 = rectangle.PMin.x, y1 = rectangle.PMin.y; 
    			var x2 = rectangle.PMax.x, y2 = rectangle.PMax.y; 
    			var m = this.pageHeight, n = this.pageWidth;

    			var xc = (x1 + x2)/2, yc = (y1 + y2)/2;
    			var xmin = xc, ymin = yc;
    			var xmax = xc, ymax = yc;

    			console.log(xc, yc, xmin, ymin, xmax, ymax);

    			this.drawNode(nodes[0], xc, yc, false, this.opacity);
    			
    			var i = 1;
    			var finish = false;

    			while(! finish)
    			{
    				xmin = xmin - this.step.x; ymin = ymin - this.step.y;
    				
    				xmax = xmax + this.step.x; ymax = ymax + this.step.y;
    				// top
    				for(var x = xmin; x <= xmax; x += this.step.x)
    				{
    					if( ! finish && (i < nodes.length) )
    					{
    						this.drawNode(nodes[i], x, ymin, false, this.opacity);
    						i++;
    					}
    					else
    					{
    						this.noMoreNodes = true;
    						finish = true;
    						break;
    					}
    				}
    				// right
    				for(var y = ymin + this.step.y; y <= ymax; y += this.step.y)
    				{
    					if( ! finish && (i < nodes.length) )
    					{
    						this.drawNode(nodes[i], xmax, y, false, this.opacity);
    						i++;
    					}
    					else
    					{
    						this.noMoreNodes = true;
    						finish = true;
    						break;
    					}
    				}
    				// bottom
    				for(var x = xmax - this.step.x; x >= xmin; x -= this.step.x)
    				{
    					if( ! finish && (i < nodes.length) )
    					{
    						this.drawNode(nodes[i], x, ymax, false, this.opacity);
    						i++;
    					}
    					else
    					{
    						this.noMoreNodes = true;
    						finish = true;
    						break;
    					}
    				}
    				// left
    				for(var y = ymax - this.step.y; y >= ymin + this.step.y; y -= this.step.y)
    				{
    					if( ! finish && (i < nodes.length) )
    					{
    						this.drawNode(nodes[i], xmin, y, false, this.opacity);
    						i++;
    					}
    					else
    					{
    						this.noMoreNodes = true;
    						finish = true;
    						break;
    					}
    				}
    			}

    			if( rectangle.PMin.x < this.map.PMin.x ) this.map.PMin.x = rectangle.PMin.x;
    			if( rectangle.PMax.x > this.map.PMax.x ) this.map.PMax.x = rectangle.PMax.x;
    			if( rectangle.PMin.y < this.map.PMin.y ) this.map.PMin.y = rectangle.PMin.y;
    			if( rectangle.PMax.y > this.map.PMax.y ) this.map.PMax.y = rectangle.PMax.y;

    			console.log('End put memories', this.map);

    			
    		},

    		loadPages : function( pages )
    		{
    			if(this.currentPage <= this.lastPage)
    			{
    				var self = this;
	    			var looper = $.Deferred().resolve();
	    			this.setMouseUI(false);
	    			// Rym.utils.putCssAjaxLoader('#burger-navigation');
	    			$.each(pages, function(i, record) {
	    				var PMin = record.rectangle.PMin, PMax = record.rectangle.PMax;
	    				looper = looper.then(function() {
				            return $.ajax({
				                data: {
				                    filterOptions : self.filterOptions,
				                    pageLength : self.pageLength,
				                    _token : Rym.utils.token(),
				                },
				                method: 'post',
				                url: Rym.utils.baseUrl(self.pageUrl + '?page=' + record.page),
				                dataType: 'json'
				            }).done(function(response) {
				            	console.log(self.filterOptions, response.where);
				                self.putMemories(response.memories.data, record.rectangle);
				                self.lastPage = response.memories.last_page;
				                self.currentPage = response.memories.current_page;
				                if( self.currentPage == 1)
				                {
				                	Rym.main.showIntro();
				                }
				                if( i + 1 == pages.length )
				                {
				                	console.log('A fost ultimul request (CP : ' + self.currentPage + ')');
				                	self.setMouseUI(true);
				                	// Rym.utils.putCssBurger();
				                }
				            });
				        });
	    			});
    			}
    			else
    			{
    				// alert('NO MORE PAGES!');
    			}
    		},

    		getFilteredNodes : function()
    		{
    			
    			var self = this;
    			// pun 0 la numararea nodurilor incarcate
    			$('#memories-count-value').html(0);
				Rym.utils.fetchData('get-number-of-filtered-nodes', {
					filterOptions : this.filterOptions
				}, 'post').then( function(response){
					if( 1 || (parseInt(response.number_of_nodes) > parseInt(self.pageLength)) )
					{
						var maxx = (self.pageWidth - 1)/2 * self.step.x, maxy = (self.pageHeight - 1)/2 * self.step.y;
						self.loadPages([
							{
								page : 1,
								rectangle : {
									PMin : {x : -maxx, y : -maxy},
								}
							},
						]);
					}
					else
					{
						alert('NU AVEM DESTULE NODURI! ' + parseInt(response.number_of_nodes));
					}
				});
				// console.log('FILTER => ', this.filterOptions, maxx, maxy, this.pageLength);
    		},

    		loadNewPages : function()
    		{
    			var self = this;
    			var renderer = this.s.renderers[0];
    			var nodesOnScreen = renderer.nodesOnScreen.length;
    			var c = this.getCamera();
    			var r = this.getRectangle( this.s.renderers[0].container.clientWidth, this.s.renderers[0].container.clientHeight );
    			var SMin = {x : r.x1, y : r.y1}, SMax = {x : r.x2, y : r.y2 + r.height};
    			var PMin = this.map.PMin, PMax = this.map.PMax;

    			//console.log('ScreenX: ' + SMax.x + ' MapX: ' + PMax.x + ' Diff: ' + (SMax.x - PMax.x));

    			if(2 * (SMax.x - PMax.x) > this.step.x)
    			{
    				var numberOfPages = 0; var y = PMin.y;
    				while( y <= PMax.y )
    				{
    					numberOfPages++; y += this.pageHeight * this.step.y;
    				}
    				var pages = [];
    				var currentPage = this.currentPage;
    				var xmin = PMax.x + this.step.x;
    				var ymin = PMin.y;
    				for(var i = 1; i <= numberOfPages; i++)
    				{
    					currentPage++;
    					pages.push({
			    			page : currentPage,
							rectangle : {PMin : {x : xmin, y : ymin},}
    					});
    					ymin += this.step.y * this.pageHeight;
    				}
    				this.loadPages(pages);
    				return;
    			}
    			if( 2 * (SMax.y - PMax.y) > this.step.y)
    			{
    				var numberOfPages = 0; var x = PMin.x;
    				while( x <= PMax.x )
    				{
    					numberOfPages++; x += this.pageWidth * this.step.x;
    				}
    				var pages = [];
    				var currentPage = this.currentPage;
    				var xmin = PMin.x;
    				var ymin = PMax.y + this.step.y;
    				for(var i = 1; i <= numberOfPages; i++)
    				{
    					currentPage++;
    					pages.push({
			    			page : currentPage,
							rectangle : {PMin : {x : xmin, y : ymin},}
    					});
    					xmin += this.step.x * this.pageWidth;
    				}
    				this.loadPages(pages);
    				return;
    			}
    			if(2 * (PMin.x - SMin.x) > this.step.x)
    			{
    				var numberOfPages = 0; var y = PMin.y;
    				while( y <= PMax.y )
    				{
    					numberOfPages++; y += this.pageHeight * this.step.y;
    				}
    				var pages = [];
    				var currentPage = this.currentPage;
    				var xmin = PMin.x - this.step.x * this.pageWidth;
    				var ymin = PMin.y;
    				for(var i = 1; i <= numberOfPages; i++)
    				{
    					currentPage++;
    					pages.push({
			    			page : currentPage,
							rectangle : {PMin : {x : xmin, y : ymin},}
    					});
    					ymin += this.step.y * this.pageHeight;
    				}
    				this.loadPages(pages);
    				return;
    			}
    			if( 2 * (PMin.y - SMin.y) > this.step.y)
    			{
    				var numberOfPages = 0; var x = PMin.x;
    				while( x <= PMax.x )
    				{
    					numberOfPages++; x += this.pageWidth * this.step.x;
    				}
    				var pages = [];
    				var currentPage = this.currentPage;
    				var xmin = PMin.x;
    				var ymin = PMin.y - this.step.y * this.pageHeight;
    				for(var i = 1; i <= numberOfPages; i++)
    				{
    					currentPage++;
    					pages.push({
			    			page : currentPage,
							rectangle : {PMin : {x : xmin, y : ymin},}
    					});
    					xmin += this.step.x * this.pageWidth;
    				}
    				this.loadPages(pages);
    				return;
    			}
    		},

			/*
			 * CAMERA SPECIFIC
			 */
			getCamera : function()
			{
				var c =  this.s.cameras[0];
				return {x : c.x, y : c.y };
			},

			goTo : function(x, y)
			{
				this.s.cameras[0].goTo({x : x, y : y});
			},

			goToNode : function(node)
			{
				var self = this;
				self.removeEdges();
				var interval = setInterval( function(){
					var c = self.getCamera();
					var x = c.x, y = c.y;				
					dist = Math.sqrt((x - node.x) * (x - node.x) + (y - node.y) *(y - node.y));
					if( dist < 10)
					{
						clearInterval(interval);
						self.goTo(node.x, node.y);
						self.handleDragEnd();
					}
					else
					{
						x = (9*x + node.x)/10; y = (9*y + node.y)/10;
						self.goTo(x, y);
					}
				}, 25);
			},

			getRectangle : function(width, height){ return this.s.cameras[0].getRectangle(width, height);},

			/* EVENTS */
			clickStage : function(e, click){ 
				// Rym.neuronal.hideTooltip();
				// console.log('MyNetwork. clickStage(). Clicked');
			},

			dragStage : function(e, click){
				// Rym.neuronal.hideTooltip();
				this.handleDragEnd();
			},

			/*
			 * Cans se face click pe un nod
			 **/
			clickNode : function(e, click)
			{
				var node = e.data.node;   // aflu nodul clickuit
				if( this.main == node.id )
				{
					return;
				}
				this.main = node.id;
				var self = this;
				this.unselectNodes();
				this.setMouseUI(false);   // Nu permit sa se mai faca click pe filtrare
				
				self.selectNode(node);    // il selectez cu un cerc
				self.goToNode(node);      // ma pozitionez pe cerc cu efect de tranzitie lent

                // Rym.utils.putCssAjaxLoader('#burger-navigation');

				// dupa ce am facut click pe un nod ==> restul sunt opace la incarcare
				this.opacity = true;
				Rym.utils.fetchData('get-memory-edges', {target : node, ids : this.getLoadedIds()}, 'post', '#map-activity').then(function(response){
					self.putEdges(response.edges);
					$('#map-activity').hide();
					Rym.neuronal.showTooltip(node);
					self.setMouseUI(true);
					Rym.utils.setBackground('#map-view', response.background, false);
					// Rym.utils.putCssBurger()
				});
			},

			dragNode : function(e, click){ 
				// console.log('MyNetwork.dragNode() > Drag node'); 
			},

			handleDragEnd : function()
			{
				this.loadNewPages();
			},

			setMouseUI : function(flag)
			{
				if( flag )
				{
					this.s.refresh();
				}
				this.s.settings('mouseEnabled', flag);
			},

			bindEvent : function(options)
			{
				var self = this;
				if( options.native )
				{
					this.s.bind(options.eventName, function(e){
						self.click = {
							PScreen  : {x : e.data.captor.clientX, y : e.data.captor.clientY},
							PMap : {x : e.data.captor.x, y : e.data.captor.x},
							isDragging : e.data.captor.isDragging,
							moment : new Date(),  
						};
						if( self.click.isDragging )
						{
							options.eventDragCallback.call(self, e, self.click);
						}
						else
						{
							options.eventClickCallback.call(self, e, self.click);
						}
					});
				}
			}
		};

		return Network;
	}());

	Network.create = function( options )
	{
		var network = new Network(options);
		// Bind native events
		network.bindEvent({
			native : true,
			eventName : 'clickStage',
			eventDragCallback : function(e, click)
			{
				network.dragStage(e, click);
			},
			eventClickCallback : function(e, click)
			{
				network.clickStage(e, click);
			},
		});
		network.bindEvent({
			native : true,
			eventName : 'clickNode',
			eventDragCallback : function(e, click)
			{
				network.dragNode(e, click);
			},
			eventClickCallback : function(e, click)
			{
				network.clickNode(e, click);
			},
		});

		network.s.bind('coordinatesUpdated', function(e){
			// network.loadNewPages();
		});

		network.s.bind('hovers', function(e) {

			/*
			 * hovered = false ==> stins
			 *         = true  ==> aprins
			 * daca nodul este deja aprins cand intru pe el ==> are opacity = false
			 * opacity = false ===> aprins (nu are opacitate)
			 */
			var n = e.data.enter.nodes; 
			for(var i = 0; i < n.length; i++)
			{
				if(! n[i].selected)
				{
					n[i].hovered = true;
				}
			}
			n = e.data.leave.nodes;
			for(var i = 0; i < n.length; i++)
			{
				if(! n[i].selected)
				{
					n[i].hovered = false;
				}
			}

			network.s.refresh();
			if(e.data.current.nodes.length > 0)
			{
				e.target.renderers[0].domElements.mouse.style.cursor = 'pointer';
			}
			else
			{
				e.target.renderers[0].domElements.mouse.style.cursor = 'default';
			}
			
		});

		var _downTouches = null, _movingTimeoutId = null, _isMoving = false, _touchMode = 0, _isDragging = 0;
		var _x, _y, _dist;

		function _handleTouchStart(e) 
	    {
	    	var touchobj = e.changedTouches[0];
	    	_x = parseInt(touchobj.clientX);
	    	_y = parseInt(touchobj.clientY);
	    	_dist = 0;

	    	_downTouches = e.touches;
	    	_touchMode = _downTouches.length;
	    	switch(_downTouches.length) 
	        {
	            case 1:
	                _touchMode = 1;
	                break;
	            case 2:
	                _touchMode = 2;
	                e.preventDefault();
	                return false;
	        }
	        // console.log('_handleTouchStart() > Start at: (' + _x + ', ' + _y + '). Mode: ' + _touchMode);
	    }

	    function _handleTouchMove(e) 
	    {
	        // e.preventDefault();
	    	var touchobj = e.changedTouches[0];	    	
	    	var _dx = parseInt(touchobj.clientX) - _x, _dy = parseInt(touchobj.clientY) - _y
	    	_dist = Math.sqrt(/* _dx * _dx + */_dy * _dy);
	    	// console.log('_handleTouchMove() > Move at: (' + _x + ', ' + _y + '), dist=' + parseFloat(_dist));
	        _downTouches = e.touches;
	        _isMoving = true;
	        if (_movingTimeoutId)
	        {
	            clearTimeout(_movingTimeoutId);
	        }
	        _movingTimeoutId = setTimeout(function() {_isMoving = false;}, 200);
	    }

	    function _handleTouchEnd(e) 
	    {
	    	
	    	// console.log('_handleTouchEnd() > Touch End at: (' + _x + ', ' + _y + '), dist=' + parseFloat(_dist));
	    	if (_movingTimeoutId)
            {
                _isMoving = false;
                clearTimeout(_movingTimeoutId);
            }
            _downTouches = e.touches;
            switch (_touchMode) 
            {
                case 2:
                    if (e.touches.length === 1) 
                    {
                    	e.preventDefault();
                        _handleTouchStart(e);
                        break;
                    }
                /* falls through */
                case 1:
                	console.log(_dist);
                	if(_dist > 0)
                	{
                		// console.log('_handleTouchEnd() > Oare este DRAG END?');
                		network.handleDragEnd();
                	}
                	else
                	{
                		// console.log('_handleTouchEnd() > ', e);
                	}
                    _isMoving = false;
                    _touchMode = 0;
                    break;
            }
            var touchobj = e.changedTouches[0];
	    	_x = parseInt(touchobj.clientX);
	    	_y = parseInt(touchobj.clientY);
            _dist = 0;
            
	    }

		var _target = $('.sigma-mouse')[0];

		_target.addEventListener('touchstart', _handleTouchStart);
		_target.addEventListener('touchmove', _handleTouchMove);
        _target.addEventListener('touchend', _handleTouchEnd);
        _target.addEventListener('touchenter', _handleTouchEnd);
        _target.addEventListener('touchleave', _handleTouchEnd);
        _target.addEventListener('touchcancel', _handleTouchEnd);
       
		return network;
	}

	window.Network = Network;

}(window, $, sigma));