Rym.neuronal = {

	setBackground : function()
	{
		var width = Rym.utils.width();
		var backgroundImage = 'assets/' + ( (width < 992) ? 'small' : 'large') +  '/neuronal-background.png';
		Rym.utils.setBackground('#map-view', backgroundImage, true);
	},

	onResize : function()
	{
		$(window).resize(function(){Rym.neuronal.setBackground();});
	},

	addTooltip : function()
	{

		// $("#slide-intro-modal").on('hidden.bs.modal', function () {
		// 	alert('S-a inchis intro');
		// });


		$('#btn-plus').tooltipster({
			side:'left',
			theme: ['tooltipster-noir', 'tooltipster-noir-customized'],
			animation: 'fade',
   			delay: 200,
   			trackOrigin : true,
		});
		
	},

	onClickSearch : function()
	{
		$('#btn-search').click(function(e){
			var bk = $('#burger-navigation').css('background-image')
			if( bk.search('burger') >= 0 )
			{
				$('#btn-plus').tooltipster('close');
				$('#search-panel-modal').modal({
					keyboard: false, 
					show : true
				});
			}
		});
	},

	onClickTooltip : function()
	{
		$(document).on('click', '#memory-tooltip', function(e){
			location.href = $('#memory-tooltip').data('url');
		});
	},

	getCountriesByContinent : function(continent_id)
	{
		Rym.utils.fetchData('get-countries-by-continent', {continent_id : continent_id}, 'post', '#filter-loading-indicator').then(function(response){
			$('#filter-loading-indicator').hide();
			$('#country_id').html( response.countries );
			$('#country_id').selectpicker('refresh');
			$('#country_id').selectpicker('val', null);
		});
	},

	onCreateMemory : function()
	{
		$('#btn-plus').click(function(e){
			Rym.utils.redirect('recreate-your-memory');
		});
	},

	onShowFilterForm : function()
	{
		$('#search-panel-modal').on('shown.bs.modal', function(e){
	        $("#years-range").ionRangeSlider({
	            hide_min_max: true,
	            keyboard: false,
	            min: $('#year-from').html(),
	            max: $('#year-to').html(),	           
	            from: $('#year-from').html(),
	            to: $('#year-to').html(),
	            type: 'double',
	            step: 1,
	            prefix: "",
	            grid: false,
	            onChange: function (data) {$('#year-from').html(data.from); $('#year-to').html(data.to);},
	        });
	        $('#continent_id, #country_id').selectpicker({});
	        $('#continent_id').selectpicker('val', null);
	        $('#continent_id').on('changed.bs.select', function(e){
	        	if( parseInt($(this).val()) )
	        	{
	        		Rym.neuronal.getCountriesByContinent($(this).val());
	        	}
	        });
	        $('#input-tags').focus();
	    });
	},

	onFilterCancel : function()
	{
		$(document).on('click', '#btn-filter-cancel', function(e){
			$('#search-panel-modal').modal('hide');
		});
	},

	onFilterReset : function()
	{
		$(document).on('click', '#btn-filter-reset', function(e){
			$('#input-tags').val('');
			$('#country_id').selectpicker('val', null);
			$('#continent_id').selectpicker('val', null);
			var slider = $("#years-range").data("ionRangeSlider");
			slider.reset();
			$('#search-panel-modal').modal('hide');
			MyNetwork.clear();
			MyNetwork.noMoreNodes = false;
			MyNetwork.filterOptions = null;
			MyNetwork.loadMemories();
			MyNetwork.goTo(0, 0);
		});
	},

	onFilterApply : function(type, callback)
	{
		var self = this;
		$(document).on('click', '#btn-filter-apply', function(e){
	    	var valid = false, tags = $('#input-tags').val(), years = $("#years-range").data("ionRangeSlider").result, country_id = $('#country_id').val();
	    	if( tags.length )
	    	{
	    		valid = true;
	    	}
	    	if( country_id )
	    	{
	    		valid = true;
	    	}
	    	if( (years.from != years.min) || (years.to != years.max) )
	    	{
	    		valid = true;
	    	}
	    	if( valid )
	    	{
	    		$('#search-panel-modal').modal('hide');
				self.hideTooltip();
	    		var filterOptions = {
		    		tags : tags,
		    		years : {from: years.from, to: years.to},
		    		country_id : country_id,
		    	};
	    		if(type == 'network-view')
	    		{
	    			MyNetwork.clear();
	    			MyNetwork.opacity = false;
	    			MyNetwork.currentPage = 1;
	    			MyNetwork.lastPage = 100000;
	    			MyNetwork.loadingNode = null;
					MyNetwork.loadedPages = [];
					MyNetwork.main = null;
		    		MyNetwork.noMoreNodes = false;
		    		MyNetwork.filterOptions = filterOptions;
			    	MyNetwork.getFilteredNodes();
			    	MyNetwork.goTo(0, 0);
			    }
			    else
			    {
			    	callback(filterOptions);
			    }
	    	}
	    	else
	    	{
	    		$('#error-messages').show('blind', {}, 500);
	    	}
	    });
	},

	updateMemoryLoadedCount : function( number )
	{
		Rym.utils.setHtml('#memories-count-value', number)
	},

	showTooltip : function(node)
	{
		// Helpers.setBackground('#map-view', node.photo, false);
		var windowWidth = $(window).width();
		var bottomControls = ( windowWidth < 992 ? 128.5 : 145.5); 
		var bottomCount = ( windowWidth < 992 ? 128.5 :  128.5); 
		var visible = ( (windowWidth < 992) ? '.view-small' : '.view-large');

		Rym.utils.setHtml('#memory-name' + visible, node.name);
		Rym.utils.setHtml('#m-location' + visible, node.country_name);
		Rym.utils.setHtml('#m-date-of' + visible, node._date_of);
		Rym.utils.setHtml('#m-likes' + visible, node.count_likes);
		Rym.utils.setHtml('#m-comments' + visible, node.count_comments);

		$('#user-avatar img').attr('src', node.photo);
		$('#memory-tooltip').data('url', node.detail_url);

		$("#map-view-controls").animate({bottom : bottomControls + 'px'}, 500 );
		$("#memories-count").animate({bottom : bottomCount + 'px'}, 500 );
		$('#memory-tooltip, #memory-tooltip-transparent').hide().show('clip', {}, 500);
	},

	hideTooltip : function()
	{
		var windowWidth = $(window).width();
		var bottomControls = ( windowWidth < 992 ? 20 : 32); 
		var bottomCount = ( windowWidth < 992 ? 20 :  20); 
		$('#memory-tooltip, #memory-tooltip-transparent').hide('clip', {}, 500);
		$("#map-view-controls").animate({bottom : bottomControls + 'px'}, 500 );
		$("#memories-count").animate({bottom : bottomCount + 'px'}, 500 );
	},

	onTooltipHover : function()
	{
		// aici un efect in timp !!!
		$(document).on('mouseover', '#memory-tooltip, #memory-tooltip-transparent', function(){
			$('#memory-tooltip-transparent').removeClass('tooltip-transparent').addClass('tooltip-opaque');
		});

		$(document).on('mouseout', '#memory-tooltip, #memory-tooltip-transparent', function(){
			$('#memory-tooltip-transparent').removeClass('tooltip-opaque').addClass('tooltip-transparent');
		});
	},

	init : function()
	{
		this.setBackground();
		this.onResize();
		this.onTooltipHover();
		this.onClickTooltip();
		this.addTooltip();
		this.onCreateMemory();
		this.onClickSearch();
		this.onShowFilterForm();
		this.onFilterCancel();
		this.onFilterReset();
	}
}

// console.log(Rym);