(function(window, sigma){

	sigma.utils.pkg('sigma.canvas.nodes');
	sigma.canvas.nodes.image = (function(){
	    
	    var _cache = {}, _loading = {}, _callbacks = {};
	    var renderer = function(node, context, settings) 
	    {
	        var 
	            args   = arguments,
	            prefix = settings('prefix') || '',
	            size   = node[prefix + 'size'],
	            color  = node.color || settings('defaultNodeColor'),
	            url    = node.url,
	            x      = node[prefix + 'x'], y = node[prefix + 'y'];


	        if( _cache[url]) 
	        {
	            context.save();
	            /*
	             * Daca nodul este selectat desenez cercul alb
	             */
	            if(node.selected)
	            {
	            	context.globalAlpha = 0.5; // transparenta mare
	            	context.beginPath();
		            context.arc(x, y, size/2 + 10, 0, Math.PI * 2, true);
		            context.lineWidth = 0.75;
		            context.strokeStyle = '#FFF';
		            context.stroke();
	            }

	            /*
	             * Draw the clipping disc (???)
	             */
	            context.beginPath();
	            context.arc(node[prefix + 'x'], node[prefix + 'y'], size/2, 0, Math.PI * 2, true);
	            context.closePath();
	            context.clip();

	            /*
	             *  Draw the image
	             *  opacity = true ==> trebuie sa fie stins
	             *  selected = true ==> trebuie sa fie luminos
	             *  hoveredd = true ==> trebuie sa fie luminos
	             *  globalAlpha = 1 ==> transparenta mica ==> luminos
	             *  globalAlpha = 0 ==> transparenta mare ==> stins
	             */
	            if(node.selected || node.hovered)
	            {
	            	context.globalAlpha = 1;
	            }
	            else
	            {
	            	if(node.opacity)
	            	{ 
	            		context.globalAlpha = 0.4;
	            	}
	            }
	            context.drawImage(_cache[url], node[prefix + 'x'] - size/2, node[prefix + 'y'] - size/2, size, size );
				context.restore(context.globalAlpha);
	        } 
	        else 
	        {
	            sigma.canvas.nodes.image.cache(url);
	            sigma.canvas.nodes.def.apply(sigma.canvas.nodes, args);
	        }
	    };

	    // Let's add a public method to cache images, to make it possible to preload images before the initial rendering.
	    renderer.cache = function(url, callback) 
	    {
	        if (callback)
	        {
	            _callbacks[url] = callback;
	        }
	        if (_loading[url])
	        {
	            return;
	        }
	        var img = new Image();
	        img.onload = function() 
	        {
	            _loading[url] = false;
	            _cache[url] = img;
	            if (_callbacks[url]) 
	            {
	                _callbacks[url].call(this, img);
	                delete _callbacks[url];
	            }
	        };
	        _loading[url] = true;
	        img.src = url;
	    };

	    return renderer;
	})();

}(window, sigma));