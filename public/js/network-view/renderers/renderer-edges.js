(function(window, sigma){

	sigma.utils.pkg('sigma.canvas.edges');

	sigma.canvas.edges.t = function(edge, source, target, context, settings) {

        var prefix = settings('prefix') || '';
		// source
	 	var x1 = source[prefix + 'x'], y1 = source[prefix + 'y'], r1 = source[prefix + 'size'];
	 	// target
	 	var x2 = target[prefix + 'x'], y2 = target[prefix + 'y'], r2 = target[prefix + 'size'];
	 	// triangle
	 	var ipotenuza = Math.sqrt( (x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1) );
	 	var sin = (y2 - y1)/ipotenuza;
	 	var cos = (x2 - x1)/ipotenuza;
	 	// source point
	 	xs = x1 + (r1/2 + 10)*cos;
	 	ys = y1 + (r1/2 + 10)*sin;
	 	// target point
	 	xt = x2 - (r2/2 + 10)*cos;
	 	yt = y2 - (r2/2 + 10)*sin;
	 	// drawing line
	 	context.save();
	 	context.lineWidth = 0.75;
	 	context.setLineDash([3, 3]);
	 	context.beginPath();
	 	context.moveTo(xs, ys);
	 	context.lineTo(xt, yt);
	 	context.strokeStyle = '#FFF';
	 	context.stroke();
	 	// aici desenez cerculetele din capetele
	 	context.globalAlpha = 1;
	 	context.beginPath();
	 	context.lineWidth = 0.5;
	 	context.strokeStyle = edge.color;
	 	context.arc(xs, ys, 3, 0, Math.PI * 2, true);
	 	context.stroke();
	 	context.fillStyle = edge.color;
	 	context.fill();	 

		context.beginPath();
		context.arc(xt, yt, 3, 0, Math.PI * 2, true);
		context.stroke();
		context.fill();

	 	context.restore();
	};
}(window, sigma));