Rym.mapview = {

	baseMarkerUrl    : Rym.utils.baseUrl('assets/map-view/marker-'),
	maxNumberOfNodes : 200,
	selectedMarker   : null,
	mapInstance      : null,
	gmap             : null,

	loaded           : false,

	setOptions : function ()
	{
		Mapview.options.center = {lat : 46.219702, lng : 24.796388};
		Mapview.options.zoom = 4;
		Mapview.options.minZoom = 1;
		Mapview.options.maxZoom = 12;		
		Mapview.options.scrollwheel = true;
		Mapview.options.draggable = true;
		// Mapview.options.disableDefaultUI = false;
	},

	initMap : function()
	{
		console.log('Start map veiw....');

		this.setOptions();
		this.mapInstance = $('#map-view').mapview(Mapview.options);
		this.gmap = $('#map-view').mapview('getMap').gMap;

		// EVENTS
		var self = this;
		google.maps.event.addListener(this.gmap, 'bounds_changed', function(){
			console.log('Bounds Changed > ');
			if( ! self.loaded )
			{
				self.loadMemories();
				self.loaded = true;
			}
		});
	},


	loadMemories : function()
	{
		var self = this;
		var coordinates = this.getCoordinates();
		Rym.utils.fetchData('gmap-load-memories', {
			coordinates : coordinates
		}, 'post').then(function(response){
			self.addMarkers(response);
		});
	},

	addMarkers : function(memories)
	{
		var self = this;

		for(var i = 0; i < memories.length; i++)
		{
			var node = memories[i];


			this.mapInstance.mapview('addMarker', {
				lat : (latitude = parseFloat(node.latitude)),
				lng : (longitude = parseFloat(node.longitude)),
				icon : this.baseMarkerUrl + 'blue.png',
				visible : true,
				id : node.id,
				animation : google.maps.Animation.DROP,
				info : new google.maps.InfoWindow({content : this.infoContent(node)}),
				node : node,
				events : [
					{
						eventName : 'click',
						eventCallback : function(e, marker){
							self.onClick(marker);
						}
					},
					{
						eventName : 'dragend',
						eventCallback : function(e){ 
							// console.log('Drag end on ' + 2);
						}
					}
				]
			});
			Rym.neuronal.updateMemoryLoadedCount(this.mapInstance.mapview('getMarkers').length);
		}
	},

	getCoordinates : function()
	{

		var bounds = this.gmap.getBounds();
		console.log(this.gmap, bounds, this.gmap.getCenter());
		var NE = bounds.getNorthEast();
		var SW = bounds.getSouthWest();
		var C = this.gmap.getCenter();
		return {
			sw : {latitude : SW.lat(), longitude : SW.lng()},
			ne : {latitude : NE.lat(), longitude : NE.lng()},
			diff : {
				latidue : NE.lat() - SW.lat(),
				longitude : NE.lng() - SW.lng(),
			},
			center : {latitude : C.lat(), longitude : C.lng()},
			zoom : $('#map-view').mapview('zoom'),
		};
	},

	infoContent : function(node)
	{
		return '<div class="info-content"><div class="info-content-left"><img src="' + node.photo + '" alt="' + node.name +'" class="img-circle" /></div><div class="info-content-right"><a class="marker-link" href="' + node.url + '">' + node.name + '</a></div></div>';
	},

	onClick : function(marker)
	{
		if( ! this.isSelected(marker) )
		{
			if(this.selectedMarker)
			{
				this.markerUnselect(this.selectedMarker);
			}
			this.markerSelect(marker);
		}
		else
		{
			this.markerUnselect(marker);
		}
	},

	isColor : function(marker, color)
	{
		return marker.icon == (this.baseMarkerUrl + color + '.png');
	},

	isSelected : function(marker, color)
	{
		return this.isColor(marker, 'yellow');
	},

	setColor : function(marker, color)
	{
		var newIcon = this.baseMarkerUrl + color + '.png';
		if(marker.icon != newIcon )
		{
			marker.icon = newIcon;
		}
	},

	markerSelect : function(marker)
	{
		this.setColor(marker, 'yellow'); 
		this.selectedMarker = marker;
		marker.setAnimation(google.maps.Animation.BOUNCE);
		marker.info.open(this.mapInstance.mapview('getMap').gMap, marker);
		Rym.neuronal.showTooltip(marker.node);
	},

	markerUnselect : function(marker)
	{
		this.setColor(marker, 'blue'); 
		marker.setAnimation(null);
		this.selectedMarker = null;
		marker.info.close();
		Rym.neuronal.hideTooltip();
	},

	removeAllMarkers : function()
	{
		this.mapInstance.mapview('removeAll')
	},

	initUI : function()
	{
		Rym.neuronal.onTooltipHover();
		Rym.neuronal.onClickTooltip();
		Rym.neuronal.addTooltip();
		Rym.neuronal.onCreateMemory();
		Rym.neuronal.onClickSearch();
		Rym.neuronal.onShowFilterForm();
		Rym.neuronal.onFilterCancel();
		Rym.neuronal.onFilterReset();
	}

}

Rym.mapview.initUI();