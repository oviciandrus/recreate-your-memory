(function(window, $){

	$(document).ready(function(){

		// http://web.archive.org/web/20160122183325/http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/docs/reference.html
		// https://developers.google.com/maps/documentation/javascript/reference


		Rym.mapview.initMap();


		return;

		/*
		 * Variabile globale
		 */
		



		// Maps.bindEvents();
		// Maps.bindApplySearch('map-view', function(filterOptions){
		// 	fetchAll(1, filterOptions, null);
		// });

		/*
		 * se instantiaza obiectul jQuery myMap
		 */
		

		return;
		/*
		 * se extrage obiectul Google Map
		 */
		

		/*
		 * Captarea evenimentelor
		 */
		

		/*
		 * MAP FUNCTIONS
		 * =============
		 */
		var getCoordinates = function()
		{
			var bounds = map.getBounds();
			var NE = bounds.getNorthEast();
			var SW = bounds.getSouthWest();
			var C = map.getCenter();
			return {
				sw : {latitude : SW.lat(), longitude : SW.lng()},
				ne : {latitude : NE.lat(), longitude : NE.lng()},
				diff : {
					latidue : NE.lat() - SW.lat(),
					longitude : NE.lng() - SW.lng(),
				},
				center : {latitude : C.lat(), longitude : C.lng()},
				zoom : $('#map-view').mapview('zoom'),
			};
		}

		/*
		 * MARKER FUNCTIONS
		 * ================
		 */
		
		




		var fetchAll = function(page, filterOptions, boundOptions)
		{ 
			if(page == 1)
			{
				removeAllMarkers();
			}
			Helpers.fetchData('map-view?page=' + page, {filterOptions : filterOptions, boundOptions : boundOptions}, 'get').then( function(response){
				
				console.log('Total: ' + response.total + '. Page: #' + response.current_page + '/#' + response.last_page, filterOptions, boundOptions,  getCoordinates());
				
				addMarkers(response.data);
				// if(page == 1)
				// {
				// 	updateMemoriesCount(response.total);
				// }
				if( (myMap.mapview('getMarkers').length < maxNumberOfNodes ) && (response.current_page < response.last_page) )
				{
					fetchAll(response.current_page + 1, filterOptions, boundOptions);
				}
				else
				{
					console.log('Markers was loaded...');
				}
			});
		}

		/*
		 * se incepe incarcarea cu pagina 1
		 */
		fetchAll(1, null, null);


		var updateMemoriesCount = function(value)
		{
			$('#memories-count-value').html(value);
		}

		var hideTooltip = function()
		{
			$('#map-view-controls').css({bottom : '0px'});
			$('#memories-count').css({bottom : '0px'});
			$('#memory-tooltip, #memory-tooltip-transparent').hide();
		}

		


		var addMarkers = function(nodes)
		{
			var latitude = 0;
			var longitude = 0;
			for(var i = 0; i < nodes.length; i++)
			{
				var node = nodes[i];

				if( myMap.mapview('getMarkers').length < maxNumberOfNodes )
				{
					myMap.mapview('addMarker', {
						lat : (latitude = parseFloat(node.latitude)),
						lng : (longitude = parseFloat(node.longitude)),
						icon : baseMarkerUrl + 'blue.png',
						visible : true,
						id : node.id,
						animation: google.maps.Animation.DROP,
						info : new google.maps.InfoWindow({content : infoContent(node)}),
						node : node,
						events : [
							{
								eventName : 'click',
								eventCallback : function(e, marker){onClick(marker);}
							},
							{
								eventName : 'dragend',
								eventCallback : function(e){ 
									// console.log('Drag end on ' + 2);
								}
							}
						]
					});
					updateMemoriesCount(myMap.mapview('getMarkers').length);
				}
				else
				{
					break;
				}
			}
			myMap.mapview('setCenter', latitude, longitude);
		}	









});
}(window, jQuery));