(function(window, $, messages, preview){

	$(document).ready(function(){

		var baseurl = $('meta[name="baseurl"]').attr('content');

		/**
		 * NAVIGATION
		 **/
		function showPage(index)
		{
			$('.create-step').hide();	
			$('#create-step-' + index).show();
			$('#navigation').ScrollTo();
		}

		$('#btn-step-1-back').click( function(e){
			location.reload();
		});

		$('#btn-step-2-back').click(function(e){
			showPage(1);
		});

		$('.go-to-page').click(function(e){
			var pageNo = $(this).data('page');

			var currentPageNo = $('.create-step:visible').data('page');
			if(pageNo == currentPageNo)
			{
				return false;
			}

			console.log('Esti la pagina #' + currentPageNo + '. Vrei direct la pagina  #' + pageNo);

			if(currentPageNo == 1)
			{
				if( pageNo == 2)
				{
					$('#btn-step-1-next').click();
				}
				return false;
			}

			if(currentPageNo == 2)
			{
				if(pageNo == 1)
				{
					showPage(1);
					return true;
				}
				if(pageNo == 3)
				{
					$('#btn-step-2-next').click();
				}
				return false;
			}

			if(currentPageNo == 3)
			{
				if(pageNo == 2)
				{
					showPage(2);
				}
			}
		});

		/**
		 * Datepicker for memory date
		 **/
		$('#datetimepicker1').datetimepicker({
			locale: 'en',
			viewMode: 'years',
			format: 'MMMM Do YYYY',
			ignoreReadonly : true
		}).on('dp.change', function(e) {$("#step-2-form").formValidation('revalidateField', 'txtDate');});

		$('#txtDate').click(function(e){
			$('#datetimepicker1').data('DateTimePicker').show();
		});
		/**
		 * Geocomplete for memory location
		 **/
		$("#txtLocation").geocomplete({
			map: "#map-preview",
			mapOptions: {
				scrollwheel : true,
				draggable : false,
				zoom : 14,
				// styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
			},
			markerOptions: {
				draggable: false,
				icon : baseurl + '/assets/small/marker-yellow.png',
			},
			details: "#map-details",
  			detailsAttribute: "data-geo"
		})
		.bind("geocode:result", function(event, result){})
      	.bind("geocode:error", function(event, status){})
        .bind("geocode:multiple", function(event, results){});

        /**
         * ion check radio for optin notifications
         */
 		$("#notification-options, #terms-newsletter-options").find("input").ionCheckRadio();

 		/**
 		 * Nicescroll
 		 */
 		var nice = $("#txtDescription").niceScroll({
 			cursorcolor : '#000064',
 		});

 		/**
 		 * Sociality login: facbook & twitter
		 */
		$('#btn-sociality-facebook, #btn-sociality-twitter').click(function(e){
			location.href = $(this).data('url');
		});

 		/**
 		 * Step #1. Dropzone for person photo
 		 */
 		var personUploader = StepOne.personUploader(preview, messages.file);

 		/**
 		 * Step #2. Dropzone for memory photo
 		 */
 		var memoryUploader = StepTwo.memoryUploader(messages.file);
 		
 		/**
 		 * Step #1 - Person form validation
 		 **/
 		var personFormValidation = StepOne.formValidation(messages);

		/**
 		 * Step #2 - Memory form validation
 		 **/
 		var memoryFormValidation = StepTwo.formValidation(messages);
		
		/**
		 * Go from Page #1 (Person) to Page #2 (Memory)
		 */
	    $('#btn-step-1-next').click(function(e){
	    	StepOne.onClickNext( function(response){showPage(2);});
	    });

		/**
		 * Go from Page #2 (Memory) to Page #3 (Success)
		 */
		$('#btn-step-2-next').click(function(e){
			StepTwo.onClickNext( function(response){
				showPage(3); 
			});	
		});

		$('#btn-step-3-view').click(function(e){
			var url = $(this).data('url');
			StepTwo.onClickSave(function(response){
				location.href = url;
			});
		});

		$('#txtDescription').focus(function(e){
			$(this).closest('div').removeClass('form-group-description').addClass('form-group-description-focused');
		});

		$('#txtDescription').blur(function(e){
			$(this).closest('div').removeClass('form-group-description-focused').addClass('form-group-description');
		})

		showPage(1);
 	});
	
}(window, $, messages, preview));