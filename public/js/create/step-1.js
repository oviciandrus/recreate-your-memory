var StepOne = 
{

	isImageUploaded : false,

	formValidation : function(messages)
	{
		return $("#step-1-form").formValidation({
	        framework:'bootstrap',
	        icon:{
	        	valid : 'glyphicon glyphicon-ok', 
	        	invalid : 'glyphicon glyphicon-remove',
	        	validating :'glyphicon glyphicon-refresh'
	        },
	        fields: {
	            txtFirstName: {
	                validators: {
	                    notEmpty: {message:messages.firstname.required},
	                    stringLength: {max: 20, message: messages.firstname.size}
	                }
	            },
	            txtLastName: {
	                validators: {
	                    notEmpty: {message:messages.lastname.required},
	                    stringLength: {max: 20, message: messages.lastname.size}
	                }
	            },
	            txtEmail: {
	                validators: {
	                    notEmpty: {message : messages.email.required},
	                    emailAddress : {message : messages.email.email}
	                }
	            },
	            txtNickname: {
	                validators: {
	                    notEmpty: {message:messages.nickname.required},
	                    stringLength: {max: 20, message: messages.nickname.size}
	                }
	            },
	        }
	    })
		// .on('err.form.fv', function(e) {
		// // $(e.target) --> The form instance
		// // $(e.target).data('formValidation') --> The FormValidation instance
		// })
		// .on('success.form.fv', function(e) {
		// })
		// .on('success.validator.fv', function(e, data) {
		// })
		// .on('err.field.fv', function(e, data) {
		// });
        ;
	},

	uploadWriteError : function(message)
	{
		$('#user-photo-upload-message').css({
			'background-color' : '#fff',
			'color' : '#cc0000'
		}).html(message).fadeIn();
	},

	uploadWriteSuccess : function(message)
	{
		$('#user-photo-upload-message').css({
			'background-color' : '#fff',
			'color' : '#000064'
		}).html(message).fadeIn();
	},

	uploadRemoveMessage : function()
	{
		$('#user-photo-upload-message').html('').fadeOut();
	},

	/*
	 * Dropzon instance for person photo
	 */
	personUploader : function(preview, messages)
	{
		if($('#upload-person-photo-form').length == 0)
		{
			return null;
		}
		// console.log('Am de la facebook', preview);
		var self = this;
		var dz = new Dropzone('#upload-person-photo-form', {
 			
 			paramName : 'person-photo',
 			parallelUploads : 1,
 			uploadMultiple : false,
 			maxFiles : 1,
 			maxFilesize: 5,
 			filesizeBase : 1024,
 			acceptedFiles : "image/*",
 			thumbnailWidth :180,
 			thumbnailHeight : 180,
 			clickable : '#user-photo-upload',
 			addRemoveLinks : true,
 			dictCancelUpload : '',
 			dictCancelUploadConfirmation : '',
 			dictRemoveFile : '',
 			dictFileTooBig : messages.size,
 			dictInvalidFileType : messages.type,

 			processing : function()
 			{
 				Rym.main.showAjax();
 				self.uploadRemoveMessage();
 				$('#user-photo-upload-progress').show();
 			},
 			
 			complete : function(file)
 			{
 				// se executa indiferent
 				// poate fi cu succes sau cu eroare

 				// indiferent error/success opresc indicatorul Ajax
 				Rym.main.hideAjax();
 				// indiferent error/succes resetez progressbar
 				$('#user-photo-upload-progress > div').css({
					width : '0%',
				}).attr('aria-valuenow', 0);
 				$('#user-photo-upload-progress').hide();
 			},

 			success: function(file, response)
		    {
		    	if(file.provider == undefined)
		    	{
			    	if( ! response.success )
			    	{
			    		self.isImageUploaded = false;
			    		self.uploadWriteError(response.message);
			    		return false;
			    	}
			    	try 
			    	{
			    		// 'Fisierul s-a incarcat (#' + $('#person_id').val() + ')' 
			    		var person = response.person;
			    		$('#upload-person-photo-form a.dz-remove').show();
	 					$('#user-photo-upload-info').removeClass('no-user-photo'); 
			    		$('#person_id').val(person.id);
			    		self.uploadWriteSuccess(messages.success + ' (#' + $('#person_id').val() + ')');
			    		self.isImageUploaded = true;
			    		Rym.utils.goTo('#user-photo-form');
			    	}
			    	catch(err) 
			    	{
			    		self.isImageUploaded = false;
			    		self.uploadWriteError(err.message);
			    	}
			    }
			    else
			    {
			    	$('#person_id').val(file.person_id);
			    	self.uploadWriteSuccess(messages.success + ' (#' + $('#person_id').val() + ')');
			    	self.isImageUploaded = true;
			    }
            },

 			error : function(file, error)
 			{
 				self.isImageUploaded = false;
 				self.uploadWriteError(error);
 				this.removeAllFiles();
		        dz.options.maxFiles = 1;
 			},

 			maxfilesexceeded: function(file) 
 			{
		        this.removeAllFiles();
		        dz.options.maxFiles = 1;
		        this.addFile(file);
		    },

			uploadprogress: function(file, progress, bytesSent) 
			{
				$('#user-photo-upload-progress > div').css({
					width : progress + '%',
				}).attr('aria-valuenow', progress);
				$('step-1-navigation button').prop('disable', parseInt(progress) < 100);
			},

		    
 		});
		dz.on('removedfile', function(file){
			self.uploadRemoveMessage();
			self.isImageUploaded = false;
		});
		if( preview )
		{
			console.log(preview);
			dz.removeAllFiles();
			dz.emit("addedfile", preview);
	        dz.createThumbnailFromUrl(preview,  preview.url);
	        dz.emit("success", preview);
	        dz.emit("complete", preview);
	        dz.files.push(preview);
	        dz.options.maxFiles = dz.options.maxFiles - 1;
			$('#upload-person-photo-form a.dz-remove').show();
			Rym.utils.goTo('#user-photo-form');
		}
		return dz;
	},

	/*
	 * Verifica daca poza "person" este ok
	 */
	validPhoto : function()
	{
		var photoIsValid = false;
		var photoUrl = null;

		if( $('#upload-person-photo-form').length > 0)
		{
			/*
			 * vad daca am poza incarcata in dropzone
			 */
    		var photoImg = $('#upload-person-photo-form > .dz-preview > .dz-image > img');
    		if( photoImg.length > 0)
    		{
    			photoUrl = photoImg.attr('src');
    			if(photoUrl)
    			{
    				photoIsValid = photoUrl.length > 0; 
    			}
    		}
    	}
    	else
    	{
    		/*
    		 * aici am poza de la Facebook/Twitter
    		 */
    		photoUrl = $('#user-socialite-avatar > img').attr('src');
    		photoIsValid = photoUrl.length > 0;
    	}
    	if(! photoIsValid || ! this.isImageUploaded)
    	{
    		$('#user-photo-upload-info').addClass('no-user-photo');
    	}
    	else
    	{
    		$('#user-photo-upload-info').removeClass('no-user-photo');
    	}
    	return { valid : photoIsValid, url : photoUrl};
	},

	/*
	 * Valideaza formularul cu nume, prenume, email, nickname
	 */
	validForm : function()
	{
		var form = $("#step-1-form").data('formValidation');
		form.validate();
		var isValid = form.isValid();
		if( ! isValid )
		{
			return {valid : false};
		}
		else
		{
			return {valid: true, data: {
				person_id : $('#person_id').val(),
				socialite_user_id : $('#socialite-user-id').val(),
				email : $('#step-1-form #txtEmail').val(),
				first_name : $('#step-1-form #txtFirstName').val(),
				last_name : $('#step-1-form #txtLastName').val(),
				nickname : $('#step-1-form #txtNickname').val(),
			}};
		}
	},

	/*
	 * creaza inregistrarea in persons
	 */
	finish : function(data, afterCallback)
	{
		var caption = $('#btn-step-1-next').find('.caption').html();
		$('#btn-step-1-next').find('.caption').html('');
		$('#btn-step-1-next').find('.indicator').css({'display':'block'}).show();
		Rym.utils.fetchData('finish-step-one', data, 'post').then(function(response){
			$('#person_id').val(response.person.id);
			$('#btn-step-1-next').find('.indicator').hide();
			$('#btn-step-1-next').find('.caption').html(caption);
			afterCallback(response);
		});
	},

	onClickNext : function( afterCallback )
	{
		this.uploadRemoveMessage();
		var photoIsValid = this.validPhoto();
    	if( photoIsValid.valid )
    	{
    		var formIsValid = this.validForm();
    		if(formIsValid.valid)
    		{
    			this.finish(formIsValid.data, afterCallback);
    		}
    		else
    		{
    		}
    	}
    	else
    	{
    	}
	}
}