var StepTwo = 
{
	isImageUploaded : false,

	uploadWriteError : function(message)
	{
		$('#memory-photo-upload-message').css({
			'background-color' : '#fff',
			'color' : '#cc0000'
		}).html(message).fadeIn();
	},

	uploadWriteSuccess : function(message)
	{
		$('#memory-photo-upload-message').css({
			'background-color' : '#fff',
			'color' : '#000064'
		}).html(message).fadeIn();
	},

	uploadRemoveMessage : function()
	{
		$('#memory-photo-upload-message').html('').fadeOut();
	},

	memoryUploader : function(messages)
	{
		if( $('#upload-memory-photo-form').length == 0)
		{
			return null;
		}
		var self = this;
		var memoryUploader = new Dropzone('#upload-memory-photo-form', {

 			paramName : 'memory-photo',
 			parallelUploads : 1,
 			uploadMultiple : false,
 			maxFiles : 1,
 			maxFilesize: 5,
 			filesizeBase : 1024,
 			acceptedFiles : "image/*",
 			thumbnailWidth : 280,
 			thumbnailHeight : 280,
 			clickable : '#memory-photo-upload',
 			addRemoveLinks : true,
 			dictCancelUpload : '',
 			dictCancelUploadConfirmation : '',
 			dictRemoveFile : '',
 			dictFileTooBig : messages.size,
 			dictInvalidFileType : messages.type,

 			processing : function()
 			{
 				Rym.main.showAjax();
 				self.uploadRemoveMessage();
 				$('#memory-photo-upload-progress').show();
 			},
 			
 			complete : function()
 			{
 				// indiferent error/success opresc indicatorul Ajax
 				Rym.main.hideAjax();
 				// indiferent error/succes resetez progressbar
 				$('#memory-photo-upload-progress > div').css({
					width : '0%',
				}).attr('aria-valuenow', 0);
 				$('#memory-photo-upload-progress').hide();
 			},

		    success: function(file, response){
		    	if( ! response.success )
		    	{
		    		self.uploadWriteError(response.message);
		    		self.isImageUploaded = false;
		    		return false;
		    	}
		    	try 
		    	{
		    		var memory = response.memory;
		    		$('#upload-memory-photo-form a.dz-remove').show();
 					$('#memory-photo-upload-info').removeClass('no-user-photo');
		    		$('#memory_id').val(response.memory.id);
		    		self.uploadWriteSuccess(messages.success + ' (#' + $('#memory_id').val() + ')');
		    		self.isImageUploaded = true;
		    		Rym.utils.goTo('#memory-info');
		    	}
		    	catch(err) 
		    	{
		    		self.uploadWriteError(err.message);
		    		self.isImageUploaded = false;
		    	}
		    },

		    error : function(file, error)
 			{
 				self.uploadWriteError(error);
 				self.isImageUploaded = false;
 				this.removeAllFiles();
		        dz.options.maxFiles = 1;
 			},

		    maxfilesexceeded: function(file) {
		        this.removeAllFiles();
		        dz.options.maxFiles = 1;
		        this.addFile(file);
		    },

		    uploadprogress: function(file, progress, bytesSent) 
			{
				$('#memory-photo-upload-progress > div').css({
					width : progress + '%',
				}).attr('aria-valuenow', progress);
				$('step-2-navigation button').prop('disable', parseInt(progress) < 100);
			},

 		});
		memoryUploader.on('removedfile', function(file){
			self.uploadRemoveMessage();
			self.isImageUploaded = false;
		});
		return memoryUploader;
	},

	formValidation : function(messages)
	{
		var fvm = $("#step-2-form").formValidation({
	        framework:'bootstrap',
	        icon:{
	        	valid : 'glyphicon glyphicon-ok', 
	        	invalid : 'glyphicon glyphicon-remove',
	        	validating :'glyphicon glyphicon-refresh'
	        },
	        fields: {
	            txtMemoryName : {
	                validators: {
	                    notEmpty: {message:messages.memoryname.required},
	                    stringLength: {max: 250, message: messages.memoryname.size}
	                }
	            },
	            txtDescription: {
	                validators: {
	                    notEmpty: {message:messages.description.required},
	                    stringLength: {max: 1000, message: messages.description.size}
	                }
	            },
	            txtLocation: {
	                validators: {
	                    notEmpty: {message : messages.location.required},
	                }
	            },
	            txtDate: {
	                validators: {
	                    notEmpty: {message : messages.date.required},
	                }
	            },
	        }
	    });
		return fvm;
	},

	validPhoto : function()
	{
		var photoIsValid = false;
		var photoUrl = null;
		if( $('#upload-memory-photo-form').length > 0)
		{
			var photoImg = $('#upload-memory-photo-form > .dz-preview > .dz-image > img');
			if( photoImg.length > 0)
			{
				var photoUrl = photoImg.attr('src');
				if(photoUrl)
				{
					photoIsValid = photoUrl.length > 0; 
				}
			}
		}
		if(! photoIsValid || ! this.isImageUploaded)
		{
			$('#memory-photo-upload-info').addClass('no-user-photo');
		}
		else
		{
			$('#memory-photo-upload-info').removeClass('no-user-photo');
		}
		return { valid : photoIsValid, url : photoUrl};
	},

	validForm : function()
	{
		var form = $("#step-2-form").data('formValidation');
		form.validate();
		var isValid = form.isValid();
		if( ! isValid )
		{
			return {valid : false};
		}
		else
		{
			return {valid: true, data: {
				person_id : $('#person_id').val(),
				memory_id : $('#memory_id').val(),
				name : $('#step-2-form #txtMemoryName').val(),
				description : $('#step-2-form #txtDescription').val(),
				country_code : $('#step-2-form #txtCountryCode').val(),
				country : $('#step-2-form #txtCountry').val(),
				latitude : $('#step-2-form #txtLatitude').val(),
				longitude : $('#step-2-form #txtLongitude').val(),
				date : $('#datetimepicker1').data('DateTimePicker').date().format('YYYY-MM-DD'),
				tags_list : $('#step-2-form #txtTags').val(),
				mailOptions : {
					terms : ($('input:checkbox[value="terms"]').prop('checked') ? 1 : 0),
					newsletter : ($('input:checkbox[value="newsletter"]').prop('checked') ? 1 : 0),
				}
			}};
		}
	},

	finish : function(data, afterCallback)
	{
		var caption = $('#btn-step-2-next').find('.caption').html();
		$('#btn-step-2-next').find('.caption').html('');
		$('#btn-step-2-next').find('.indicator').css({'display':'block'}).show();
		$('#finish-message-memory').html('').fadeOut();
		Rym.utils.fetchData('finish-step-two', data, 'post').then(function(response){
			$('#btn-step-2-next').find('.indicator').hide();
			$('#btn-step-2-next').find('.caption').html(caption);
			if(! response.success)
			{
				$('#finish-message-memory').html(response.message).fadeIn();
				return;
			}
			afterCallback(response);
		});
	},

	saveMemory : function(afterCallback)
	{
		var data = {
			person_id : $('#person_id').val(),
			memory_id : $('#memory_id').val(),
			mailOptions : {
				sameCountry : ($('input:checkbox[value="1"]').prop('checked') ? 1 : 0),
				sameYear : ($('input:checkbox[value="2"]').prop('checked') ? 1 : 0),
				sameTags : ($('input:checkbox[value="3"]').prop('checked') ? 1 : 0),
				comments : ($('input:checkbox[value="4"]').prop('checked') ? 1 : 0),
			}
		};
		var caption = $('#btn-step-3-view').find('.caption').html();
		$('#btn-step-3-view').find('.caption').html('');
		$('#btn-step-3-view').find('.indicator').css({'display':'block'}).show();
		$('#save-message-memory').html('').fadeOut();
		Rym.utils.fetchData('save-memory', data, 'post').then(function(response){
			$('#btn-step-3-view').find('.indicator').hide();
			$('#btn-step-3-view').find('.caption').html(caption);
			if(! response.success)
			{
				$('#save-message-memory').html(response.message).fadeIn();
				return;
			}
			afterCallback(response);
		});
	},

	onClickNext : function( afterCallback )
	{
		var photoIsValid = this.validPhoto();
		if( photoIsValid.valid )
		{
			var formIsValid = this.validForm();
			if(formIsValid.valid)
			{
				if( $('input[name="terms-options"]').prop('checked') )
				{
					this.finish(formIsValid.data, afterCallback);
				}
				else
				{
					$('#terms-newsletter-options label.icr-label:first-child span.icr-text').addClass('agree-error');
					Rym.utils.goTo('#terms-newsletter-options');
				}
			}
			else
			{
    			Rym.utils.goTo('#memory-info');
			}
		}
		else
		{
    		Rym.utils.goTo('#memory-photo-upload');
		}
	},

	onClickSave : function(afterCallback)
	{
		this.saveMemory(afterCallback);
	}

}