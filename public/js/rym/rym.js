var Rym = {};

Rym.utils = {

	baseUrl : function(path)
	{
		if( path )
		{
			return $('meta[name="baseurl"]').attr('content') + '/' + path;
		}
		return $('meta[name="baseurl"]').attr('content');
	},

	token : function()
	{
		return $('meta[name="csrftoken"]').attr('content');
	},

	fetchData : function(url, data, type, indicator)
	{
		if(indicator)
		{
			$(indicator).show();
		}
		data._token = this.token();
		return $.ajax({data : data, url : this.baseUrl(url), type : type}); 
	},

	redirect : function(path)
	{
		location.href = this.baseUrl(path);
	},

	reload : function()
	{
		location.reload;
	},

	setBackground : function(selector, file, withUrl )
	{
		$(selector).backstretch((withUrl ? this.baseUrl() + '/' : '') + file);
	},

	height : function()
	{
		return $(window).height();
	},

	width : function()
	{
		return $(window).width();
	},

	setHtml : function(selector, text)
	{
		$(selector).html(text);
	},

	goTo : function(selector)
	{
		$(selector).ScrollTo();
	},

	isVisible : function(selector)
	{
		return $(selector).is(':visible');
	},

	show : function(selector)
	{
		return $(selector).show('clip', {}, 500);	
	},

	hide : function(selector)
	{
		return $(selector).hide('clip', {}, 500).hide();
	},

	centerModal : function( modal )
	{
		var $clone = modal.clone().css('display', 'block').appendTo('body');
		var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
        top = top > 0 ? top : 0;
        $clone.remove();
        modal.find('.modal-dialog').css("top", top);        
	},

	putCssBackraoundImage : function(selector, image)
	{
		var oldBackGround = $(selector).css('background-image');
		$(selector).css( {'background-image' : 'url("' + Rym.utils.baseUrl(image) +'")'});
		return oldBackGround
	},

	putCssAjaxLoader : function(selector)
	{
		
		// if( this.width() < 992 )
		// {
		// 	return this.putCssBackraoundImage(selector, 'assets/small/wheel.gif');
		// }
		// return this.putCssBackraoundImage(selector, 'assets/large/wheel.gif');
	},

	putCssBurger : function()
	{
		// if( this.width() < 992 )
		// {
		// 	return this.putCssBackraoundImage('#burger-navigation', 'assets/small/burger.png');
		// }
		// return this.putCssBackraoundImage('#burger-navigation', 'assets/large/burger.png');
	}

}

Rym.main = {

	currentTipItem : 0,
	introWasShowed : false, 

	onClickBurger : function()
	{
		$('#burger-navigation').click(function(e){

			var bk = $(this).css('background-image');
			if( bk.search('burger') >= 0 )
			{
				if($('#btn-plus').length)
				{
					$('#btn-plus').tooltipster('close');
				}
				$('#burger-menu-modal').modal({
					keyboard: false, 
					show : true
				});
			}
		});
	},

	onShowBurgerForm : function()
	{
		$('#burger-menu-modal').on('shown.bs.modal', function(e){
	        $('#language_id').selectpicker({});
	    });
	},

	onSelectLanguage : function()
	{
		$(document).on('change', '#language_id', function(){
			location.href = Rym.utils.baseUrl('select-language/' + $(this).val());
		});
	},

	showIntro : function()
	{
		if( ! this.introWasShowed )
		{
			$('#slide-intro-modal').modal({
				keyboard: false, 
				show : true,
				backdrop : 'static',
			});
		}
	},

	onSkipIntro : function()
	{
		$(document).on('click', '#skip a', function(e){
			e.preventDefault();
			$('#btn-plus').tooltipster('open');
			$('#slide-intro-modal').modal('hide');
		});
	},

	onClickLearnMore : function()
	{
		$(document).on('click', '#btn-learn-more', function(e){
			if($('#btn-plus').length)
			{
				$('#btn-plus').tooltipster('close');
			}
			$('#slide-intro-modal').modal('hide');
			$('#slide-tips-modal').modal({
				keyboard: false, 
				show : true,
				backdrop : 'static',
			});
		});
	},

	onClickTip : function()
	{
		var self = this;
		$(document).on('click', '.btn-tip-item', function(e){
			self.currentTipItem = $(this).data('page');
			self.showTipPage(self.currentTipItem);
		});
	},

	onClickClosetip : function()
	{
		$(document).on('click', '#btn-close', function(e){
			$('#slide-tips-modal').modal('hide');
			$('#btn-plus').tooltipster('open');
		});
	},

	onClickTipLeft : function()
	{
		var self = this;
		$(document).on('click', '#btn-left', function(e){

			self.currentTipItem = (self.currentTipItem + 3) % 4;
						console.log('L' + self.currentTipItem);
			self.showTipPage(self.currentTipItem);
		});
	},

	onClickTipRight : function()
	{
		var self = this;
		$(document).on('click', '#btn-right', function(e){
			self.currentTipItem = (self.currentTipItem + 1) % 4;
			console.log('R' + self.currentTipItem);
			self.showTipPage(self.currentTipItem);
		});
	},

	onClickGetStart : function()
	{
		$(document).on('click', '#btn-get-started', function(e){
			$('#slide-tips-modal').modal('hide');
			location.href = Rym.utils.baseUrl('recreate-your-memory');
		});
	},

	showTipPage : function(page)
	{
		if(page == 0)
		{
			$('#btn-left').hide();
		}
		else
		{
			$('#btn-left').show();
		}
		if(page == 3)
		{
			$('#btn-right').hide();
		}
		else
		{
			$('#btn-right').show();
		}

		$('.btn-tip-item').removeClass('btn-tip-item-selected').addClass('btn-tip-item-unselected');
		$('.tips-page').hide();
		$('#btn-go-to-page-' + page).addClass('btn-tip-item-selected').removeClass('btn-tip-item-unselected');
		$('#tips-page-' + page).fadeIn(1500);

	},

	onClickLike : function()
	{
		$(document).on('click', '.btn-like', function(e){
			if( ! $(this).hasClass('is-liked') )
			{
				var memory_id = $(this).data('memory-id') ;
				Rym.utils.fetchData('like-memory', {memory_id:memory_id}, 'post').then( function(response){
					if(response.success)
					{
						var cnt = parseInt( $('.memory-likes').html() ) + 1; 
						$('.memory-likes').html(cnt).addClass('is-liked');
						$(this).addClass('is-liked').addClass('no-pointer');
					}
				});
			}
		});
	},

	showAjax : function()
	{
		$('#burger').hide();
		$('#ajax-loader').show();
	},

	hideAjax : function()
	{
		$('#ajax-loader').hide();
		$('#burger').show();
	},

	init : function()
	{

		$(".modal-transparent").on('show.bs.modal', function () {
			setTimeout( function() {
				$(".modal-backdrop").addClass("modal-backdrop-transparent"); }, 0);
		});
		$(".modal-transparent").on('hidden.bs.modal', function () {
			$(".modal-backdrop").addClass("modal-backdrop-transparent");
		});
		
		$('#slide-intro-modal').on('show.bs.modal', function(e) {
		    Rym.utils.centerModal($(this));
		});
		$('#slide-intro-modal').on('hidden.bs.modal', function(e) {
			Rym.main.introWasShowed = true;
		});

		$(window).on('resize', function(){
			if( $('#slide-intro-modal:visible').length )
			{
				Rym.utils.centerModal($('#slide-intro-modal'));
			}
		}); 

		var loading = false;

        $.ajaxSetup({
            beforeSend : function()
            {
                loading = true;
                setTimeout(function(){
                    if(loading) 
                    {
                    	Rym.main.showAjax();
                    }
                }, 50);    
            },
            complete : function()
            {
                loading = false;
                Rym.main.hideAjax();
            }
        });

		this.onClickBurger();
		this.onShowBurgerForm();
		this.onSelectLanguage();
		// this.showIntro();
		this.onSkipIntro();
		this.onClickLearnMore();
		this.onClickTip();
		this.onClickTipLeft();
		this.onClickTipRight();
		this.onClickClosetip();
		this.showTipPage(this.currentTipItem);
		this.onClickGetStart();
		this.onClickLike();
	}
}

Rym.main.init();