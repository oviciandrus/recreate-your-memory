var ComptechSoft = ComptechSoft || {};


ComptechSoft.Routing = {
	
	baseUrl : function(path)
	{
		if( path )
		{
			return $('meta[name="_url"]').attr('content') + '/' + path;
		}
		return $('meta[name="_url"]').attr('content');
	},

	redirect : function(path)
	{
		location.href = this.basUrl(path);
	},

	reload : function()
	{
		location.reload;
	}

};

ComptechSoft.Token = function()
{
	return $('meta[name="_token"]').attr('content');
}