(function(window, $){

	$(document).ready(function(){

		setTimeout(function(){
	        $('#email, #password').removeAttr('readonly');
	    }, 100);

		var fv = $("#login-form").formValidation({
	        framework:'bootstrap',
	        icon:{
	        	valid:'glyphicon glyphicon-ok',
	        	invalid:'glyphicon glyphicon-remove',
	        	validating:'glyphicon glyphicon-refresh'
	        },
	        fields: {
	    
	            email: {
	                validators: {
	                    notEmpty:{message:'aaaa'},
	                    emailAddress:{message:'bbb'},
	                    blank:{}
	                }
	            },

	            password: {
	                validators: {
	                    notEmpty:{message:'ccc'},
	                }
	            },

	        }
	    });

	});
}(window, $));