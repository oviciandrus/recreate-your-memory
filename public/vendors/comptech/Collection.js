(function(window){

	var Collection = (function(){

		function Collection(options)
		{
			this.items = [];
		}

		Collection.prototype = {
			add : function(item)
			{
				this.items.push(item);
			},

			remove : function(item)
			{
				var index = this.items.indexOf(item);
				if(index != -1)
				{
					this.items.splice(index, 1);
					return true;
				}
				return false;
			},

			removeAll : function()
			{
				this.items = [];
			},

			find : function(callback)
			{
				var callback_result;
				var items = this.items;
				var length = this.items.length;
				var result = [];
				for(var i = 0; i < length; i++)
				{
					callback_result = callback(items[i], i);
					if(callback_result)
					{
						result.push(items[i]);
					}
				}
				return result;
			},

			length : function()
			{
				return this.items.length
			}
		};

		return Collection;
	}());

	Collection.create = function(options)
	{
		return new Collection(options);
	}

	window.Collection = Collection;
}(window));