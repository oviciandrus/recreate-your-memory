<?php namespace Admin\Auth;


class Auth 
{

	public function authenticate($credentials)
	{
		$result = new \StdClass();
		try
		{
			$user = \Sentinel::authenticate($credentials);
			if( ! $user )
			{
				throw new \Exception('User not found');
			}
			$result->success = true;
			$result->next = redirect(route('admin-dashboard'));
		}
		catch(\Exception $e)
		{
			$result->success = false;
			$result->next = redirect( route('admin-login'))->withInput($credentials)->withErrors([$e->getMessage()]);
		}
		return $result;
	}

	public function logout()
	{
		$result = new \StdClass();
		try
		{
			\Sentinel::logout(null, true);
			$result->success = true;
			$result->next = redirect(route('admin-login'));
		}
		catch(\Exception $e)
		{
			$result->success = false;
			$result->next =  back()->withErrors([$e->getMessage()]);
		}
		return $result;
	}
}


/*
try 
        {
            $user = $this->
        } 
        catch (\Exception $e) 
        {
            return $this->returnException($e);
        }
        if ($user) 
        {
            return new SuccessReply(trans('auth-login.user-logout'));
        }
        return new FailureReply(trans('auth-login.user-logout-failure'));