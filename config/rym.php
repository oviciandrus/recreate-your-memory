<?php


return [
	'points' => [
		'views' => 1,
		'likes' => 10,
		'comments' => 20,
	],

	'scale' => [
		'relevance_min' => 100,
		'relevance_max' => 300,
		'diameter_min' => 80,
		'diameter_max' => 240,

		'step' => 300
	],

	'edge_colors' => [
		'year' => '#ffcc00',    // galben
		'country' => '#0000FF', // albastru
		'tags' => '#FFFFFF'     // alb
	],



];