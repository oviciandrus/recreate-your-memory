<?php namespace App\Models\Countries;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

	protected $table = 'countries';
    protected $guarded = ['id'];


    public static function getCountriesByContinent($continent_id)
    {
    	$countries = \App\Models\Memories\VCountryMemoriesCount::where('continent_id', $continent_id)->orderBy('country_name')->get();

    	return $countries->map(function($item, $i){
    		return '<option value="' . $item->country_id . '">' . $item->country_name . ' (' . $item->memories_count . ')</option>';
    	})->implode('');
    }

    public static function findByCodeOrCreate($code, $name)
    {
    	$record = self::where('country_code', $code)->first();
    	if($record)
    	{
    		$record->country_name = $name;
    		$record->save();
    		return $record;
    	}
    	$record = self::where('country_name', $name)->first();
    	if($record)
    	{
    		$record->country_code = $code;
    		$record->save();
    		return $record;
    	}
    	return self::create(['country_code' => $code, 'country_name' => $name]);
    }
}
