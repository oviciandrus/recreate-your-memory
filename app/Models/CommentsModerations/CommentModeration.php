<?php namespace App\Models\CommentsModerations;

use Illuminate\Database\Eloquent\Model;

class CommentModeration extends Model
{

	protected $table = 'comments_moderations';
    protected $guarded = ['id'];

}
