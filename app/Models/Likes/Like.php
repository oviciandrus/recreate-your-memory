<?php namespace App\Models\Likes;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{

	protected $table = 'likes';
    protected $guarded = ['id'];

}
