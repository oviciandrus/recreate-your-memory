<?php namespace App\Models\MemoriesTags;

use Illuminate\Database\Eloquent\Model;

class MemoryTag extends Model
{

	protected $table = 'memory_tag';
    protected $guarded = ['id'];

}
