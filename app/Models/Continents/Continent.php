<?php namespace App\Models\Continents;

use Illuminate\Database\Eloquent\Model;

class Continent extends Model
{

	protected $table = 'continents';
    protected $guarded = ['id'];

}
