<?php namespace App\Models\Edges\Traits;

trait EdgeRelationshipsTrait
{
    
    public function source()
    {
        return $this->belongsTo(\App\Models\Memories\Memory::class, 'source_id');
    }

    public function target()
    {
        return $this->belongsTo(\App\Models\Memories\Memory::class, 'target_id');
    }

}

