<?php namespace App\Models\Edges;

use Illuminate\Database\Eloquent\Model;
use App\Models\Edges\Traits\EdgeRelationshipsTrait;
use App\Models\Memories\Memory;

class Edge extends Model
{

    use 
        EdgeRelationshipsTrait
    ;

	protected $table = 'edges';
    protected $guarded = ['id'];

    public static function findByNodes(Memory $source, Memory $target)
    {
        $where = '(source_id = ' . $source->id . ') AND (target_id = ' . $target->id . ')';
    	return self::whereRaw($where)->first();
    }

    public static function createEdge(Memory $source, Memory $target, $colors)
    {
    	if($source->id == $target->id)
    	{
    		return NULL;
    	}
        if($source->id > $target->id)
        {
            $t = $source;
            $source = $target;
            $target = $t;
        }

        $edge = self::findByNodes($source, $target);
    	
        if(! $edge )
    	{
    		$edge = new Edge();
            $edge->source_id = $source->id;
            $edge->target_id = $target->id;
            if( $source->haveSameCountry($target) )
            {
                $edge->country_id = $source->country_id;
                $edge->country_name = $source->country->country_name;
            }
            if( $source->haveSameYear($target) )
            {   
                $edge->year = $source->year;
            }
            if( $tags = $source->commonsTags($target) )
            {
                $edge->tags = $tags;
            }
            if($edge->year)
            {
                $edge->color = $colors['year'];
            }
            else
            {
                if($edge->country_id)
                {
                    $edge->color = $colors['country'];
                }
                else
                {
                    if($edge->tags)
                    {
                        $edge->color = $colors['tags'];
                    }
                }
            }
            if($edge->color)
            {
                $edge->save();
                return $edge;
            }
    	}
        return NULL;
    }

}
