<?php namespace App\Models\MemoriesModerations;

use Illuminate\Database\Eloquent\Model;

class MemoryModeration extends Model
{

	protected $table = 'memories_moderations';
    protected $guarded = ['id'];

    public static function getApprovedMemoryIds()
    {
    	return self::where('approved', 1)->get();
    }

}
