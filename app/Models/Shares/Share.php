<?php namespace App\Models\Shares;

use Illuminate\Database\Eloquent\Model;

class Share extends Model
{

	protected $table = 'shares';
    protected $guarded = ['id'];

}
