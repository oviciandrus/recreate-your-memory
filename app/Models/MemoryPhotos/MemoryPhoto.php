<?php namespace App\Models\MemoryPhotos;

use Illuminate\Database\Eloquent\Model;

class MemoryPhoto extends Model
{

	protected $table = 'memory_photos';
    protected $guarded = ['id']; 
    
}
