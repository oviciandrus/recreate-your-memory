<?php namespace App\Models\Memories\Traits;

trait MemoryRelevanceTrait
{
    /*
     * Different sizes of memory photos based on relevance.
     * Number of views (1 view = 1 relevance point).
     * Number of likes (1 like = 10 relevance points).
     * Number of comments (1 comment = 20 relevant points).
     * There is a maximum size for a memory to avoid one memory takes the full screen
     */
    protected function calculateRealRelevance()
    {
        $points = \Config::get('rym.points');

        return 
            $points['views'] * $this->views->count() + 
            $points['likes'] * $this->likes->count() + 
            $points['comments'] * $this->comments()->where()->count();
    }

    /*
     * Photo Diameter
     */
    protected static function calculateViewRelevance($relevance) 
    {

    	$scale = \Config::get('rym.scale');
        foreach($scale as $name => $value)
        {
            $$name = $value;
        }
        if( $relevance <= $relevance_min )
        {
            return $diameter_min;
        }
        if( $relevance >= $relevance_max )
        {
            return $diameter_max;
        }
        $diameter = $diameter_min + (float) ($diameter_max - $diameter_min)/($relevance_max - $relevance_min) * ($relevance - $relevance_min);

        return (int) $diameter;
    }

    public function updateRelevance()
    {
        $this->count_views = $this->views->count();
        $this->count_likes = $this->likes->count();
        $this->count_comments = $this->comments()->whereHas('moderation', function($q){
            $q->where('approved', 1);
        })->count();
        $points = \Config::get('rym.points');
        $this->real_relevance =
            $points['views'] * $this->count_views + 
            $points['likes'] * $this->count_likes + 
            $points['comments'] * $this->count_comments;
        $this->view_relevance = self::calculateViewRelevance($this->real_relevance);
        $this->save();
    }
}

