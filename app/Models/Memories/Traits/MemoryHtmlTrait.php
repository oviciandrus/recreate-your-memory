<?php namespace App\Models\Memories\Traits;

use App\Models\Edges\Edge;

trait MemoryHtmlTrait
{




    /*
     * Genereaza html-ul ce se afiseaza jos in tooltip
     */
    public function memoryDetails($width)
    {
        return view('network-view._tooltip')->withMemory($this)->withWidth($width)->render();
    }

    // public static function renderNumberOfMemories()
    // {
    // 	return view('network-view._number_of_memories')->withNumberOfMemories(static::where('is_approved', 1)->count())->render();
    // }

    public function photoConnenctRender(Edge $edge = NULL)
    {
        return view('detail-view._connection-photo')->withMemory($this)->withEdge($edge)->render();
    }

    public static function renderControls()
    {
    	// return view('network-view.sigma._controls')->render();
    }
}

