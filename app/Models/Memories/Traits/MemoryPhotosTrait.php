<?php namespace App\Models\Memories\Traits;

trait MemoryPhotosTrait
{

    public function byDimensions($width, $height)    
    {
        return $this->photos()->where('width', $width)->where('height', $height)->first();
    }

    public function deletePhoto($width, $height)
    {
        return $this->photos()->where('width', $width)->where('height', $height)->delete();
    }

    public function createPhoto($width, $height, $prefix, $same_scale = false, $blur = false)
    {

        $photo = $this->byDimensions($width, $height);
        if( ! $photo )
        {
            $extension = \File::extension($this->photo);
            if(! $extension)
            {
                $extension = 'jpg';
            }

            $file = str_replace(\URL::to('/'), public_path(), $this->photo);
            $file = str_replace('\\', '/', $file);

            $url = 'photos/' . $prefix . '-' . $this->id . '-' . $width . '-' . $height . '.' . $extension;
            $new_file_name = str_replace('\\', '/', public_path()) . '/' . $url;
            $img = \Image::make($file);

            if($blur)
            {
                $img->blur(75);
            }

            $real_factor = $img->width() / $img->height();
            $factor = $width/$height;

            if( $same_scale )
            {
                if( $width > $height)
                {
                    $img->resize($width, (int) ($width*$real_factor), function ($constraint) {$constraint->aspectRatio(); });
                }
                else
                {
                    $img->resize((int) ($height*$real_factor), $height, function ($constraint) {$constraint->aspectRatio(); });
                }
            }
            else
            {
                if( $width > $height)
                {
                    $img->resize($width, (int) ($width*$real_factor), function ($constraint) {$constraint->aspectRatio(); });
                }
                else
                {
                    $img->resize((int) ($height*$real_factor), $height, function ($constraint) {$constraint->aspectRatio(); });
                }
                $size = min($img->width(), $img->height());
                $img->resizeCanvas($size, $size, 'center');
            }
            $img->save($new_file_name);
            $photo = $this->photos()->create([
                'height' => $height,
                'width' => $width,
                'file_name' => \URL::to($url)
            ]);
        }
        return $photo->file_name; 
    }
}

