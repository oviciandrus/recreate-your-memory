<?php namespace App\Models\Memories\Traits;

trait MemoryAttributesTrait
{
    public function getDateOfPostAttribute()
    {
        return $this->year;
    }

    public function getToCreatedAtAttribute()
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('g:i A, j F Y');
    }

    public function getTagsToListAttribute()
    {
    	if(! $this->tags )
    	{
    		return NULL;
    	}
    	return $this->tags->map(function($item, $i){ return '#' . $item->tag; })->implode(', ');
    }

    public function getLocationAttribute()
    {
        return $this->country->country_name;
    }

    public function getDateOfAttribute()
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d', $this->date)->format('j F Y');
    }

    public function getNumberOfLikesAttribute()
    {
        return $this->likes->count();
    }

    public function getNumberOfCommentsAttribute()
    {
        /*
         * only approved comments
         */
        return $this->comments()->whereHas('moderation', function($q){$q->where('approved', 1);})->count();
    }

}
