<?php namespace App\Models\Memories\Traits;

trait MemoryGettersTrait
{

    public static function makeBoundWhereRaw($bound_options)
    {
        if( ! $bound_options)
        {
            return NULL;
        }
        $result = '( (latitude >=  ' . $bound_options['sw']['latitude'] . ')  AND (latitude <= ' . $bound_options['ne']['latitude'] . ') AND (longitude >= ' . $bound_options['sw']['longitude'] . ') AND (longitude <= ' . $bound_options['ne']['longitude'] . ') )';
        return $result;
    }

    public static function makeFilterWhereRaw($filter_options)
    {
        if( ! $filter_options )
        {
            return '(1)';
        }
        $result = [];
        if($filter_options['country_id'])
        {
            $result[] = '(country_id = ' . $filter_options['country_id'] . ')';
        }
        $result[] = '(year between ' . $filter_options['years']['from'] . ' and ' . $filter_options['years']['to'] . ')'; 
        if( $filter_options['tags'] )
        {
            $t = '(';
            foreach(explode(', ', $filter_options['tags']) as $i => $tag)
            {
                $t .= "(tags_list LIKE '%" . $tag . "%') OR ";
            }
            $t = substr($t, 0, -4) . ')';
            $result[] = $t;
        }
        return '(' . collect($result)->implode(' AND ') . ')';
    }

    /*
     * Se foloseste in map-view
     */
    public static function getMarkers($coordinates)
    {
        $where = self::makeBoundWhereRaw($coordinates);
        $result = self::select(['id', 'slug_name', 'latitude', 'longitude', 'name', 'photo', 'country_name', 'date',  'count_likes', 'count_comments'])->whereRaw($where)->get();

        foreach($result as $i => $memory)
        {
            $memory->_date_of = $memory->date_of;
            $memory->detail_url = route('detail-view', ['slug_name' => $memory->slug_name, 'memory_id' => $memory->id]);
        }
        return $result;
    }
    // public static function getMarkers($filter_options, $bound_options, $pageLength)
    // {
    //     $whereFilter = self::makeFilterWhereRaw($filter_options);
    //     $whereBound = self::makeFilterWhereRaw($bound_options);

    //     ;

    //     if(! $whereFilter && ! $whereBound )
    //     {
    //         $result = $result->orderBy('real_relevance', 'desc')->paginate($pageLength);
    //     }
    //     else
    //     {
    //         if($whereFilter && $whereBound)
    //         {

    //         }
    //         else
    //         {
    //             if($whereFilter)
    //             {
    //                 $result = $result->whereRaw($whereFilter)->paginate($pageLength);
    //             }
    //             else
    //             {

    //             }
    //         }
    //     }
    //     foreach($result as $i => $memory)
    //     {
    //         $memory->_date_of = $memory->date_of;
    //         $memory->detail_url = route('detail-view', ['slug_name' => $memory->slug_name, 'memory_id' => $memory->id]);
    //     }
    //     return $result;
    // }

    /*
     * se foloseste in neuronal-view
     */
    public static function getNewNodes($filter_options, $loaded_ids, $number_of_nodes)
    {

        $whereFilter = self::makeFilterWhereRaw($filter_options);
        $whereIds = $loaded_ids ? '(NOT (id IN (' . implode(', ', $loaded_ids) . '))) ' : '(1)';
        $whereRaw = $whereFilter . ' AND ' . $whereIds;

        $result = 
            self::select(['id', 'slug_name', 'latitude', 'longitude', 'name', 'photo', 'country_name', 'date', 'view_relevance', 'count_likes', 'count_comments'])
            ->whereRaw($whereRaw)
            ->orderBy('created_at', 'desc')
            ->take($number_of_nodes)
            ->get();
        // !!! sa calculeze count likes si count comments in tabela memories
        foreach($result as $i => $memory)
        {
            $memory->_date_of = $memory->date_of;
            $memory->detail_url = route('detail-view', ['slug_name' => $memory->slug_name, 'memory_id' => $memory->id]);
        }
        return $result;
    }

    /*
     * Am facut click pe un nod
     * Vreau sa obtin nodurile conectate cu $this aflate intre ids
     */
    public function getEdgesToShow($target, $ids)
    {
        $colors = \Config::get('rym.edge_colors');
        foreach($ids as $i => $id)
        {
            $memory = self::find($id);
            \App\Models\Edges\Edge::createEdge($this, $memory, $colors);
        }
        $where1 = "(source_id = " . $this->id . ") AND (target_id IN (" . implode(',', $ids) . "))";
        $where2 = "(target_id = " . $this->id . ") AND (source_id IN (" . implode(',', $ids) . "))";
        $edges = \App\Models\Edges\Edge::whereRaw('((' . $where1 . ') OR (' . $where2 . '))')->orderByRaw('RAND()')->take(20)->get();
        return $edges;
    }

}

