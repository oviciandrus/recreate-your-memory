<?php namespace App\Models\Memories\Traits;

trait MemoryRelationshipsTrait
{
    
    public function moderation()
    {
        return $this->hasOne(\App\Models\MemoriesModerations\MemoryModeration::class, 'memory_id');
    }

    public function views()
    {
        return $this->hasMany(\App\Models\Views\View::class, 'memory_id');
    }

    public function likes()
    {
        return $this->hasMany(\App\Models\Likes\Like::class, 'memory_id');
    }

    public function comments()
    {
        return $this->hasMany(\App\Models\Comments\Comment::class, 'memory_id');
    }

    public function country()
    {
        return $this->belongsTo(\App\Models\Countries\Country::class, 'country_id');
    }

    public function tags()
    {
        return $this->belongsToMany(\App\Models\Tags\Tag::class, 'memory_tag', 'memory_id', 'tag_id');
    }   

    public function person()
    {
        return $this->belongsTo(\App\Models\Persons\Person::class, 'user_id');
    }

    public function photos()
    {
        return $this->hasMany(\App\Models\MemoryPhotos\MemoryPhoto::class, 'memory_id');
    }

}

