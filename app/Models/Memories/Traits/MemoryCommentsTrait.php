<?php namespace App\Models\Memories\Traits;

trait MemoryCommentsTrait
{

    public function getComment($data)
    {
        
        try
        {
            $person = \App\Models\Persons\Person::findByEmail($data['email']);
            
            $comment = $this->comments()->create([
                'user_id' => $person->id,
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'nickname' => $data['nickname'],
                'comment' => $data['comment']
            ]);
            $comment->moderation()->create([
                'approved' => 0,
            ]);
            $this->updateRelevance();
            return ['success' => true];
        }
        catch(\Exception $e)
        {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    public function isCommented($ip)
    {
        return (bool) $this->likes()->where('client_ip', $ip)->first();
    }

}