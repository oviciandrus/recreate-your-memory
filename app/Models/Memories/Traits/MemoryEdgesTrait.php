<?php namespace App\Models\Memories\Traits;

use App\Models\Memories\Memory;

trait MemoryEdgesTrait
{

    

    public function haveSameCountry(Memory $memory)
    {
        if( ! $this->country_id )
        {
            return false;
        }
        if( ! $memory->country_id )
        {
            return false;
        }
        if( ! ($this->country_id == $memory->country_id) )
        {
            return false;
        }
        return true;
    }

    public function haveSameYear(Memory $memory)
    {
        if( ! $this->year )
        {
            return false;
        }
        if( ! $memory->year )
        {
            return false;
        }
        if( ! ($this->year == $memory->year) )
        {
            return false;
        }
        return true;
    }

    public function commonsTags(Memory $memory)
    {
        $source_tags = $this->tags->map(function($tag){return $tag->tag; })->toArray();
        $target_tags = $memory->tags->map(function($tag){return $tag->tag; })->toArray();
        $intersect = array_intersect($source_tags, $target_tags);
        if( count($intersect) == 0 )
        {
            return NULL;
        }
        return implode(', ', $intersect);
    }

    public function getEdges($limit = 10)
    {
        $result = Edge::with(['source', 'target'])->whereRaw('(source_id = ' . $this->id . ') OR (target_id = ' . $this->id . ')')->whereNotNull('color')->limit($limit)->get();
        return $result;
    }
}

