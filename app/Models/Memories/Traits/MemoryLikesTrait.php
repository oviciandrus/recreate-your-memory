<?php namespace App\Models\Memories\Traits;

trait MemoryLikesTrait
{

    public function getLike($ip)
    {
        try
        {
            $this->likes()->create(['client_ip' => $ip]);
            $this->updateRelevance();
            return ['success' => true];
        }
        catch(\Exception $e)
        {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    public function isLiked($ip)
    {
        return (bool) $this->likes()->where('client_ip', $ip)->first();
    }

}