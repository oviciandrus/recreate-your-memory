<?php namespace App\Models\Memories\Traits;

trait MemoryViewsTrait
{

    public function getView($ip)
    {
        try
        {
            $this->views()->create(['client_ip' => $ip]);
            $this->updateRelevance();
            return ['success' => true];
        }
        catch(\Exception $e)
        {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    public function isViewd($ip)
    {
        return (bool) $this->views()->where('client_ip', $ip)->first();
    }

}