<?php namespace App\Models\Memories\Traits;

trait MemoryProcessingTrait
{
    /*
     * creez legaturile nodului $this cu celelalte
     */

	// /*
	//  * Scan all memories and set is_approved flag from memory_moderation table
	//  */
 //    public static function setApprovedFlag($skip, $take)
 //    {
 //        ini_set('max_execution_time', 0);

 //        $memories = self::skip($skip)->take($take)->get();
 //        foreach($memories as $recno => $memory)
 //        {
 //            $memory->update(['is_approved' => $memory->moderation->approved]);
 //        }
           
 //    }

 //    /*
	//  * Scan all memories and set values od views, likes, comments, relevances
	//  */
 //    public static function setRelevance($skip, $take)
 //    {
 //        ini_set('max_execution_time', 0);

 //        $points = \Config::get('rym.points');
 //        $memories = self::skip($skip)->take($take)->get();
        
 //        foreach($memories as $recno => $memory)
 //        {
 //            $count_views = $memory->views->count();
 //            $count_likes = $memory->likes->count();
 //            $count_comments = $memory->comments->count();

 //            $real_relevance = 
 //                $points['views'] * $count_views + 
 //                $points['likes'] * $count_likes + 
 //                $points['comments'] * $count_comments;

 //            $view_relevance = self::calculateViewRelevance($real_relevance);

 //            $memory->update([
 //                'count_views' => $count_views,
 //                'count_likes' => $count_likes,
 //                'count_comments' => $count_comments,

 //                'real_relevance' => $real_relevance,
 //                'view_relevance' => $view_relevance
 //             ]);
 //        }
 //    }



    public static function createSlugs()
    {
        ini_set('max_execution_time', 0);

        self::chunk(500, function ($memories){
            foreach($memories as $recno => $memory)
            {
                if( ! $memory->slug_name )
                {
                    $memory->slug_name = str_slug($memory->name, '-');
                    $memory->save();
                }
            }
        });
    }

    public static function createTags()
    {
        ini_set('max_execution_time', 0);

        self::chunk(500, function ($memories){
            foreach($memories as $recno => $memory)
            {
                if(! $memory->tags_list)
                {
                    if($memory->tags->count())
                    {
                        $memory->tags_list = $memory->tags_to_list;
                    }
                    else
                    {
                        $memory->tags_list = NULL;
                    }
                }
                $memory->save();
            }
        });
    }

 //    public static function createFakePhotos($id_min, $id_max)
 //    {
 //        ini_set('max_execution_time', 0);

 //        foreach($memories = self::whereRaw("photo like '%lorempixel%'")->where('id', '>=', $id_min)->where('id', '<=', $id_max)->orderBy('id')->get() as $i => $memory)
 //        {

 //            echo $memory->id;
 //            try 
 //            {
 //                copy($memory->photo, str_replace('\\', '/', public_path()) . '/images/fakes/' . $memory->id . '.png');
 //                $memory->photo = \URL::to('images/fakes/' . $memory->id . '.png');
 //                $memory->save();
 //                echo '....OK';
 //            } 
 //            catch(\Exception $e) 
 //            {
 //                echo '....' . $e->getMessage();
 //            }
 //            echo '<br/>';
 //        }
 //    }


}

