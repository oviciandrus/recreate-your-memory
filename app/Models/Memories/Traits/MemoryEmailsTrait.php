<?php namespace App\Models\Memories\Traits;

trait MemoryEmailsTrait
{

    public function sendThankYouEmail()
    {
        $this->email_photo = $this->createPhoto(280, 280, 'M');
        $this->country_name = $this->country->country_name;

        \App\Models\Mails\Mail::sendTo(
        	[
        		['email' =>$this->person->email, 'name' => $this->first_name . ' ' . $this->last_name],
        		['email' => 'ovidiu.andrus@comptech.ro', 'name' => 'Andrus Ovidiu',],
        		['email' => 'oviciandrus@yahoo.com', 'name' => 'Ovidiu Andrus',],
        		['email' => 'ovidiu.andrus@gmail.com', 'name' => 'O. Andrus'],
        	],
        	'emails.thank-you',
        	['memory' => $this],
        	'Thank you'
        );
    }
}

