<?php namespace App\Models\Memories;

use Illuminate\Database\Eloquent\Model;
use App\Models\Memories\Traits\MemoryRelationshipsTrait;
use App\Models\Memories\Traits\MemoryHtmlTrait;
use App\Models\Memories\Traits\MemoryAttributesTrait;
use App\Models\Memories\Traits\MemoryGettersTrait;
use App\Models\Memories\Traits\MemoryRelevanceTrait;
use App\Models\Memories\Traits\MemoryEdgesTrait;
use App\Models\Memories\Traits\MemoryPhotosTrait;
use App\Models\Memories\Traits\MemoryLikesTrait;
use App\Models\Memories\Traits\MemoryViewsTrait;
use App\Models\Memories\Traits\MemoryCommentsTrait;
use App\Models\Memories\Traits\MemoryProcessingTrait;
use App\Models\Memories\Traits\MemoryEmailsTrait;
use App\Models\Edges\Edge;
use App\Models\Persons\Person;
use App\Models\Tags\Tag;
use App\Models\Countries\Country;

class Memory extends Model
{

	use 
        MemoryRelationshipsTrait,
        MemoryHtmlTrait,
        MemoryAttributesTrait,
        MemoryGettersTrait,
        MemoryRelevanceTrait,
        MemoryEdgesTrait,
        MemoryPhotosTrait,
        MemoryLikesTrait,
        MemoryViewsTrait,
        MemoryCommentsTrait,
        MemoryEmailsTrait,
        MemoryProcessingTrait
    ;

	protected $table = 'memories';
    protected $guarded = ['id'];

    /*
     * sterge "memories" care nu au fost finalizate
     */
    public static function clearGarbage()
    {
        $records = self::whereRaw("Left(status,4) = 'temp'")->delete();
    }
 
    public static function uploadMemoryPhoto($person_id, \Illuminate\Http\UploadedFile $file)
    {
        try
        {
            if( ! $person_id )
            {
                throw new \Exception( trans('create.validations.step-2.no-person'), 1);
            }
            if( ! $file->isValid())
            {
                throw new \Exception( trans('create.validations.file.invalid'), 1);
            }
            $file_extension = $file->getClientOriginalExtension();
            $file_name = $file->getClientOriginalName();
            $base_name = basename($file_name, '.' . $file_extension);
            $destination_path = str_replace('\\', '/', public_path()) . '/images/fakes/';
            $faker = \Faker\Factory::create();
            $person = Person::find($person_id);
            $memory = Memory::create([
                'user_id' => $person->id,
                'nickname' => $person->nickname,
                'first_name' => $person->first_name,
                'last_name' => $person->last_name,
                'name' => ($sentence = $faker->sentence),
                'slug_name' => str_slug($sentence),
                'description' => $faker->text,
                'country_id' => NULL,
                'latitude' => NULL,
                'longitude' => NULL,
                'year' => NULL,
                'date' => NULL,
                'is_approved' => 0,
                'count_views' => 0,
                'count_likes' => 0,
                'count_comments' => 0,
                'real_relevance' => 0,
                'view_relevance' => 0,
                'tags_list' => NULL,
                'status' => 'temp.upload',
            ]);
            $versioned_name = $memory->id . '-original-memory-' . time() . '---' . $base_name;


            \Image::make($file)->save( $saved_file = ($destination_path . $versioned_name . '.' . $file_extension) ); 
            
            $memory->photo = str_replace(str_replace('\\', '/', public_path()),  \URL::to('/')  , $saved_file) ;
            $memory->save();
            $result = ['success' => true, 'memory' => $memory];  
        }
        catch(\Exception $e)
        {
            $result = ['success' => false, 'message' => $e->getMessage()];
        }
        return $result;
    }


    public static function finishStepTwo($data)
    {
        ini_set('max_execution_time', 0);
        try
        {
            $person = Person::find($data['person_id']);
            if( ! $person )
            {
                throw new \Exception( '1' . trans('create.validations.step-2.no-person'), 1);
            }
            $memory = self::find($data['memory_id']);
            if( ! $memory )
            {
                throw new \Exception( '2' . trans('create.validations.step-2.no-memory'), 1);
            }
            $person->terms_conditions = $data['mailOptions']['terms'];
            $person->newsletter_brand_optin = $data['mailOptions']['newsletter'];
            $person->status = 'OK';
            $person->save();
            /* --------------------------------------------------------- */
            $memory->first_name = $person->first_name;
            $memory->last_name = $person->last_name;
            $memory->nickname = $person->nickname;
            $memory->name = $data['name'];
            $memory->slug_name = str_slug($data['name']);
            $memory->description = $data['description'];
            $memory->latitude = $data['latitude'];
            $memory->longitude = $data['longitude'];
            $memory->date = $data['date'];
            $memory->year = substr($data['date'], 0, 4);
            $memory->view_relevance = 80;
            $memory->tags_list = collect(explode(',', str_replace([',', ' ', '#'], ',', $data['tags_list'])))->filter(function($item, $i){
                return strlen(trim($item)) > 0;
            })->map(function($item) use($memory){
                $item = trim($item);
                if( $item )
                {
                    $tag = Tag::findByNameOrCreate($item = str_replace('#', '', strtolower(trim($item))));
                    try
                    {
                        $memory->tags()->attach($tag);
                    }
                    catch(\Exception $e){}
                    return '#' . $item;
                } 
            })->implode(', ');
            $country = Country::findByCodeOrCreate($data['country_code'], $data['country']);
            $memory->country_id = $country->id;
            $memory->status = 'OK';
            $memory->save();
            try
            {
                $memory->moderation()->create(['approved' => 0]); // !!! aici trebuie 0
            }
            catch(\Exception $e){}
            $result = [
                'success' => true,
                'person' => $person,
                'memory' => $memory,
            ];
        }
        catch(\Exception $e)
        {
            $result = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }
        return $result;
    }

    public static function saveMemory($data)
    {
        try
        {
            $person = Person::find($data['person_id']);
            if( ! $person )
            {
                throw new \Exception( '1' . trans('create.validations.step-2.no-person'), 1);
            }
            $memory = self::find($data['memory_id']);
            if( ! $memory )
            {
                throw new \Exception( '2' . trans('create.validations.step-2.no-memory'), 1);
            }
            $person->notification_same_country = $data['mailOptions']['sameCountry'];
            $person->notification_same_year = $data['mailOptions']['sameYear'];
            $person->notification_same_tags = $data['mailOptions']['sameTags'];
            $person->notification_comments = $data['mailOptions']['comments']; 
            $person->status = 'OK';
            $person->save();
            $memory->sendThankYouEmail();
            $result = [
                'success' => true,
                'person' => $person,
                'memory' => $memory,
            ];
        }
        catch(\Exception $e)
        {
            $result = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }
        return $result;
    }
}
