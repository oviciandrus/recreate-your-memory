<?php namespace App\Models\Comments;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

	protected $table = 'comments';
    protected $guarded = ['id'];


    public function moderation()
    {
        return $this->hasOne(\App\Models\CommentsModerations\CommentModeration::class, 'comment_id');
    }

    public function person()
    {
        return $this->belongsTo(\App\Models\Persons\Person::class, 'user_id');
    }

    public function getToCreatedAtAttribute()
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('g:i A, j F Y');
    }
    
}
