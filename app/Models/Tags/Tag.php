<?php namespace App\Models\Tags;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

	protected $table = 'tags';
    protected $guarded = ['id'];


    public static function findByNameOrCreate($tag)
    {
    	$record = self::where('tag', $tag)->first();
    	if($record)
    	{
    		return $record;
    	}
    	return self::create(['tag' => $tag]);
    }

}
