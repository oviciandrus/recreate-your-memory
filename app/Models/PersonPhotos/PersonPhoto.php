<?php namespace App\Models\PersonPhotos;

use Illuminate\Database\Eloquent\Model;

class PersonPhoto extends Model
{

	protected $table = 'person_photos';
    protected $guarded = ['id']; 
    
}
