<?php namespace App\Models\Persons;

use Illuminate\Database\Eloquent\Model;
use App\Models\Memories\Traits\MemoryPhotosTrait;

class Person extends Model
{

	use 
        MemoryPhotosTrait
    ;

	protected $table = 'persons';
    protected $guarded = ['id'];

    public function photos()
    {
        return $this->hasMany(\App\Models\PersonPhotos\PersonPhoto::class, 'person_id');
    }

    /*
     * sterge "persons" care nu au fost finalizate
     */
    public static function clearGarbage()
    {
        $records = self::whereRaw("Left(status,4) = 'temp'")->delete();
    }

    public static function findByEmail($email)
    {
    	$person = self::where('email', $email)->first();
    	if( ! $person )
    	{
    		$person = self::create(['email' => $email]);
    	}
    	return $person;
    }

    public static function fromFacebook($user)
    {
        $raw = $user->getRaw();
        if( ! ($person = self::findByEmail($email = $user->getEmail())) )
        {
            $person = new Person();
        }
        $person->first_name = $raw['first_name'];
        $person->last_name = $raw['last_name'];
        $person->nickname = strtolower($raw['first_name'] . '.' . $raw['last_name']);
        $person->photo = $user->avatar_original;
        $person->status = 'temp.facebook';
        $person->save();
        // $person->deletePhoto(180, 180);
        $person->photo_preview = $person->createPhoto(180, 180, 'P');
        return $person;
    }

    public static function fromTwitter($user)
    {
        $raw = $user->getRaw();
        if( ! ($person = self::findByEmail($email = $user->getEmail())) )
        {
            $person = new Person();
        }
        $person->first_name = collect(explode(' ', $user->name))->first();
        $person->last_name =collect(explode(' ', $user->name))->last();
        $person->nickname = $user->nickname;
        $person->status = 'temp.twitter';
        $person->photo = $user->avatar_original;
        $person->save();
        // $person->deletePhoto(180, 180);
        $person->photo_preview = $person->createPhoto(180, 180, 'P');
        return $person;
    }

    public static function uploadPersonPhoto(\Illuminate\Http\UploadedFile $file)
    {
        try
        {
            if(! $file->isValid())
            {
                throw new \Exception( trans('create.validations.file.invalid'), 1);
            }
            $file_extension = $file->getClientOriginalExtension();
            $file_name = $file->getClientOriginalName();
            $base_name = basename($file_name, '.' . $file_extension);
            $destination_path = str_replace('\\', '/', public_path()) . '/images/fakes/';
            $faker = \Faker\Factory::create();
            $person = Person::create([
                'email' => $faker->email . time(),
                'nickname' => $faker->userName,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'status' => 'temp.upload',
            ]);
            $versioned_name = $person->id . '-original-person-' . time() . '---' . $base_name;
            \Image::make($file)->save( $saved_file = ($destination_path . $versioned_name . '.' . $file_extension) ); 
            $person->email = $person->id . '-' . $person->email;
            $person->photo = str_replace(str_replace('\\', '/', public_path()),  \URL::to('/')  , $saved_file) ;
            $person->save();   
            $result = ['success' => true, 'person' => $person];         
        }
        catch(\Exception $e)
        {
            $result = ['success' => false, 'message' => $e->getMessage()];
        }
        return $result;
    }

    public static function finishStepOne($data)
    {
        $person = self::findByEmail($data['email']);
        if($data['socialite_user_id'] != $data['person_id'])
        {
            $fake_person = self::find( $data['person_id']);
            $person->photo = $fake_person->photo;
        }
        $person->first_name = $data['first_name'];
        $person->last_name = $data['last_name'];
        $person->nickname = $data['nickname'];
        $person->status = 'temp.finish-step-1';
        $person->save();
        $person->deletePhoto(60, 60);
        $person->createPhoto(60, 60, 'P');
        return $person;
    }
}
