<?php namespace App\Models\Commands;

use Illuminate\Database\Eloquent\Model;

class Command extends Model
{

	protected $table = 'commands';
    protected $guarded = ['id'];

}
