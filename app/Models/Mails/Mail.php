<?php namespace App\Models\Mails;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{

	protected $table = 'mails_log';
    protected $guarded = ['id'];


    public static function sendTo($addreses, $view, $data, $subject)
    {
        $from_address = \Config::get('mail.from.address');
        $from_name = \Config::get('mail.from.name');

       	foreach($addreses as $i => $address)
        {
            \Mail::send($view, $data, function ($m) use ($address, $subject, $from_address, $from_name) {
                $m->from($from_address, $from_name);
                $m->to($address['email'], $address['name'])->subject($subject);
            });
        }
    }
}
