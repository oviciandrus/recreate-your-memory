<?php

/*
 * Neuronal View
 */
Route::get('/', 'NeuronalViewController@index')->name('neuronal-view');
Route::get('select-language/{lang}', 'LanguageController@selectLanguage')->name('select-language');

/*
 * Get more nodes ?????
 */
// Route::post('get-new-nodes', 'NeuronalViewController@getNewNodes');
Route::post('neuronal-view-get-page', 'NeuronalViewController@getPage');
Route::post('get-number-of-filtered-nodes', 'NeuronalViewController@countFiltered');
/*
 * Get filtered nodes
 */ 
// Route::post('get-filtered-nodes', 'NeuronalViewController@getFilteredNodes');

/*
 * Get edges for a node
 */
Route::post('get-memory-edges', 'NeuronalViewController@getMemoryEdges');

/*
 * Helper. Get countries by a continent
 */
Route::post('get-countries-by-continent', 'NeuronalViewController@getCountriesByContinent');

/* 
 * Detail View 
 */
Route::get('memory-details/{slug_name}~{memory_id}', 'DetailViewController@index')->name('detail-view');
Route::post('like-memory', 'DetailViewController@like')->name('like-a-memory');
Route::post('create-comment', 'DetailViewController@comment')->name('comment-a-memory');

/**
GOOGLE MAP
***/

Route::get('map-view', 'MapController@index')->name('map-view');
Route::post('gmap-load-memories', 'MapController@loadMemories')->name('gmap-load-memories');
/**
CREATE A MEMORY
*/

Route::get('recreate-your-memory/{provider?}', 'CreateController@index')->name('create');

Route::get('login-with-facebook', 'CreateController@redirectToFacebook')->name('login-with-facebook');
Route::get('login-with-twitter', 'CreateController@redirectToTwitter')->name('login-with-twitter');
Route::post('upload-person-photo', 'CreateController@uploadPersonPhoto')->name('upload-person-photo');
Route::post('finish-step-one', 'CreateController@finishStepOne')->name('finish-step-one');

Route::post('upload-memory-photo', 'CreateController@uploadMemoryPhoto')->name('upload-memory-photo');
Route::post('finish-step-two', 'CreateController@finishStepTwo')->name('finish-step-two');
Route::post('save-memory', 'CreateController@saveMemory')->name('save-memory');

/**
ADMIN
**/
Route::group(['namespace' => 'Admin', 'prefix' => 'moderation'], function() {

	Route::get('login', 'LoginController@index')->name('admin-login');
	Route::post('login', 'LoginController@login')->name('login-attempt');
	Route::get('logout', 'LoginController@logout')->name('admin-logout');

	Route::get('dashboard', 'DashboardController@index')->name('admin-dashboard');

	Route::get('memories', 'MemoriesController@index')->name('admin-memories');
	Route::post('memories-data', 'MemoriesController@tableData')->name('admin-memories-data');

	Route::get('persons', 'PersonsController@index')->name('admin-persons');
	Route::post('persons-data', 'PersonsController@tableData')->name('admin-persons-data');
});