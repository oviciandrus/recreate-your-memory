<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LanguageController extends Controller
{


	public function selectLanguage($lang)
	{
		\Session::put('lang', $lang);
        return redirect()->back()->withCookie(cookie()->forever('rym-lang', $lang));
	}


}
