<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Edges\Edge;
use App\Models\Memories\VApprovedMemory as Memory;

class DetailViewController extends Controller
{

	/**
	 * Detail View
	 **/
	public function index($slug_name, $memory_id)
	{
		ini_set('max_execution_time', 0);

		/**
		 * Locate the memory record
		 */
		$memory = Memory::where('slug_name', $slug_name)->where('id', $memory_id)->first();
		if( ! $memory )
		{
			return redirect( route('neuronal-view') );
		}
		
		/*
		 * Paginate the memory comments 
		 */
		$comments = $memory->comments()->orderBy('created_at')->whereHas('moderation', function($q){
			$q->where('approved', 1);
		})->paginate(3);

		/*
		 * Actualizez numarul de comentarii
		 */
		$_memory = \App\Models\Memories\Memory::find($memory_id);
		$_memory->count_comments = $comments->total();
		$_memory->save();
		

		/**
		 * update number of views
		 **/
		$memory->getView(request()->ip());

		/**
		 * Am nevoie de 4 noduri conectate la nodul curent
		 * Ma uit in Edges (asta trebuie sa fie creata)
		 */
		$connected = Edge::whereRaw('(source_id=' . $memory->id . ') OR (target_id=' . $memory->id . ')')->orderBy('updated_at', 'desc')->paginate(4);	

		if($connected->count() < 4)
		{
			$populars = Memory::where('id', '<>', $memory->id)->orderBy('real_relevance', 'desc')->take(4 - $connected->count())->get();
		}
		else
		{
			$populars = null;
		}

		/**
		 * photos
		 **/
		$background = $memory->createPhoto(1280, 800, 'M', true, true);
		$memory_photo = $memory->createPhoto(480, 480, 'M');
		$user_photo = $memory->person->createPhoto(60, 60, 'P');
		/**
		 * Next comments page
		 **/
		if(request()->ajax())
		{
			return response()->json([
				'page-large' => view('detail-view._comments-page-large')->with([
					'comments' => $comments, 'k' => $comments->count()
				])->render(),
				'page-small' => view('detail-view._comments-page-small')->with([
					'comments' => $comments, 'k' => $comments->count()
				])->render(),
				'next-page-url' => str_replace( \URL::to('/') . '/', '', $comments->nextPageUrl()),
			]);
		}

		return 
			view('detail-view.index')
			->withPhotos([
				'background' => $background,
				'photo480'  => $memory_photo,
				'user60' => $user_photo,		
			])
			->withMemory($memory)
			->withComments($comments)
			->withConnected($connected)
			->withPopulars($populars)
			->withIsLiked($memory->isLiked(request()->ip()))
			->withNextCommentsUrl( str_replace( \URL::to('/') . '/', '', $comments->nextPageUrl()) )
		;
	}

	public function like(Request $request)
	{
		$memory = \App\Models\Memories\Memory::find($request->get('memory_id'));
		if(! $memory)
		{
			return response()->json(['success' => false]);
		}
		return response()->json($memory->getLike($request->ip()));
	}

	public function comment(Request $request)
	{
		$memory = \App\Models\Memories\Memory::find($request->get('memory_id'));
		if(! $memory)
		{
			return response()->json(['success' => false]);
		}
		return response()->json($memory->getComment($request->except(['_token', 'memory_id']) /* + ['client_ip' => $request->ip()] */));
	}

}
