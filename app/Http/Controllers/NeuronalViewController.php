<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Memories\VApprovedMemory as Memory;
// use App\Models\Edges\Edge;

class NeuronalViewController extends Controller
{

	protected $number_of_nodes_per_batch = 69;

	/*
	 * Neuronal View
	 */
	public function index()
	{
		return view('network-view.index')->with([
			'count_nodes' => Memory::count(),
			'year_from' => 1970,
			'year_to' => \Carbon\Carbon::now()->format('Y')
		]);
	}

	public function getPage(Request $request)
	{
		ini_set('max_execution_time', 0);
		// Atentie la filtrare
		$where = Memory::makeFilterWhereRaw($request->get('filterOptions'));
		$memories = Memory::whereRaw($where)->orderBy('created_at', 'desc')->paginate($request->get('pageLength'));
		$size = \Config::get('rym.scale.diameter_max');
		foreach($memories as $i => $memory)
        {
        	$_memory = \App\Models\Memories\Memory::find($memory->id);
        	$_memory->updateRelevance();

        	$memory->real_relevance = $_memory->real_relevance;
        	$memory->view_relevance = $_memory->view_relevance;
        	
            $memory->_date_of = $memory->date_of;
            $memory->photo = $memory->createPhoto($size, $size, 'M');
            $memory->detail_url = route('detail-view', ['slug_name' => $memory->slug_name, 'memory_id' => $memory->id]);
        }
        return response()->json(['memories' => $memories, 'where' => $where]);
	}

	public function countFiltered(Request $request)
	{
		$where = Memory::makeFilterWhereRaw($request->get('filterOptions'));
		return response()->json(['number_of_nodes' => Memory::whereRaw($where)->count()]);
	}

	public function getMemoryEdges(Request $request)
	{
		ini_set('max_execution_time', 0);
		$target = $request->get('target');
		$memory = Memory::find($target['id']);

		return response()->json([
			'edges' => $memory->getEdgesToShow($target, $request->get('ids')),
			'background' => $memory->createPhoto(1280, 800, 'M', true, true),
		]);
	}

	// public function getNewNodes(Request $request)
	// {
	// 	return Memory::getNewNodes($request->get('filterOptions'), $request->get('loadedIds'), $request->get('numberOfNodes'));
	// }

	public function getCountriesByContinent(Request $request)
	{
		return response()->json([
			'countries' => \App\Models\Countries\Country::getCountriesByContinent( $request->get('continent_id') )
		]);
	}

}
