<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Models\Memories\Memory;

class MemoriesController extends Controller
{
	function index()
	{
		return view('admin.memories.index');
	}

	/**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function tableData()
    {
        return Datatables::of(Memory::query())->make(true);
    }
}