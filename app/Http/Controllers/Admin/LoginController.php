<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Admin\Auth\Auth;

class LoginController extends Controller
{

	public function __construct()
    {
        $this->middleware('sentinel.guest', ['except' => 'logout']);
    }

	public function index()
	{
		return 
			view('admin.login.index')
		;
	}

	public function login(Request $request)
	{
		$validator = \Validator::make($credentials = $request->except('_token'), [
			'email'      => 'required|email',
			'password'   => 'required',
		]);
		if($validator->fails())
		{
			return ['success' => false, 'errors' => $valid->errors()];
		}
		return (new Auth())->authenticate($credentials)->next;
	}

	public function logout()
	{
		return (new Auth())->logout()->next;
	}
}