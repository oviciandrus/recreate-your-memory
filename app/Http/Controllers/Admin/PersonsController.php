<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Models\Persons\Person;

class PersonsController extends Controller
{
	function index()
	{
		return view('admin.persons.index');
	}

	/**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function tableData()
    {
        return Datatables::of(Person::query())->make(true);
    }
}