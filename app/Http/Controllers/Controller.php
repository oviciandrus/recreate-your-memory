<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;


    public function __construct()
    {
    	\App::setLocale( $this->getLanguage() );
    }

    public function getLanguage()
    {
        $result = 'en';
        $cookie_lang = request()->cookie('rym-lang');
        if( $cookie_lang  )
        {
            $result = $cookie_lang;
        }
        else
        {
            $session_lang = \Session::get('lang');
            if($session_lang)
            {
                $result = $session_lang;
            }
        }
        \Session::put('lang', $result);
        return $result;
    }
}
