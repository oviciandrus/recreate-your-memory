<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Memories\Memory;
use App\Models\Persons\Person;

class CreateController extends Controller
{

	public function index($provider = NULL)
	{
		try
		{
			if( ! $provider )
			{
				return view('create.index')->with([
					'person' => NULL
				]);
			}
			if( $provider == 'facebook' )
			{
				$user = \Socialite::driver('facebook')->fields([
					'first_name', 'last_name', 'email', 'gender', 'birthday'
	        	])->user();
	        	$person = Person::fromFacebook($user);
			}
			else
			{
				if( $provider == 'twitter' )
				{
					$user = \Socialite::driver('twitter')->user();
					$person = Person::fromTwitter($user);
				}
			}

			$file = str_replace(\URL::to('/'),  public_path(), $person->photo_preview);
			$file = str_replace('\\', '/', $file);
			$img = \Image::make($file);

			return view('create.index')->with([
				'person' => $person,
				'preview' => [
					'name' => basename($file),
					'size' => $img->filesize(),
					'url' => $person->photo_preview,
					'provider' => $provider,
					'person_id' => $person->id,
				]
			]);
		}
		catch(\Laravel\Socialite\Two\InvalidStateException $e)
		{
			return redirect( route('create') );
		}
		catch(\League\OAuth1\Client\Credentials\CredentialsException $e)
		{
			return redirect( route('create') );
		}
		catch(\Exception $e)
		{
			return redirect( route('create') );
		}
	}

	public function redirectToFacebook()
    {
        return 
	        \Socialite::driver('facebook')->fields([
	            'first_name', 'last_name', 'email', 'gender', 'birthday'
	        ])->scopes([
	            'email', 'user_birthday'
	        ])->redirect();
    }

    public function redirectToTwitter()
    {
        return \Socialite::driver('twitter')->redirect();
    }

    public function uploadPersonPhoto(Request $request)
    {
    	$file = $request->file('person-photo');
    	return response()->json(Person::uploadPersonPhoto($file));
    }

    public function finishStepOne(Request $request)
    {
    	return response()->json(['person' => Person::finishStepOne($request->except('_token')) ]);
    }

    public function uploadMemoryPhoto(Request $request)
    {
    	$file = $request->file('memory-photo');
    	$person_id = $request->get('person_id');
    	return response()->json(Memory::uploadMemoryPhoto($person_id, $file));
    }

    public function finishStepTwo(Request $request)
    {
    	return response()->json(Memory::finishStepTwo($request->except('_token')));
    }

    public function saveMemory(Request $request)
    {
    	return response()->json(Memory::saveMemory($request->except('_token')));
    }
}
