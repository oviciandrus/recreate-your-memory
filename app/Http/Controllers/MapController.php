<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Memories\VApprovedMemory;
// use App\Models\Edges\Edge;

class MapController extends Controller
{

	protected $number_of_nodes_per_batch = 529;

	public function index(Request $request)
	{
		/*
		if($request->ajax())
		{
			$boundOptions = $request->get('boundOptions');
			$filterOptions = $request->get('filterOptions');
			return VApprovedMemory::getMarkers($filterOptions, $boundOptions, $this->number_of_nodes_per_batch);
		}
		*/
		return view('map-view.index')->with([
			'year_from' => 1970,
			'year_to' => \Carbon\Carbon::now()->format('Y')
		]);
	}

	public function loadMemories(Request $request)
	{
		// dd($request->get('coordinates'));
		return VApprovedMemory::getMarkers($request->get('coordinates'));
	}

}
