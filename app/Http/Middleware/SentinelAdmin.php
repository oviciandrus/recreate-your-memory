<?php namespace App\Http\Middleware;

use Comptech\Helpers\Replies\Exception as ExceptionReply;

class SentinelAdmin
{
    public function handle($request, \Closure $next)
    {
        // nu avem user logat
        if( ! \Sentinel::check() ) 
        {
            // ==> neautorizat | login
            if ($request->ajax()) 
            {
                return response()->json(['error' => 'unauthorized'], 401);
            } 
            else 
            {
                return redirect(route('admin-login'));
            }
        }
        // avem user logat
        // nu este administrator
        if( ! \Sentinel::getUser()->inRole('administrator') ) 
        {
            // il dam afara
            \Sentinel::logout(null, true);
            
            // neautorizat | login
            if ($request->ajax()) 
            {
                return response()->json(['error' => 'unauthorized'], 401);
            } 
            else 
            {
                return redirect(route('admin-login'));
            }
        }
        return $next($request);
    }

}
