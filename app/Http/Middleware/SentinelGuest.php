<?php namespace App\Http\Middleware;

class SentinelGuest
{

    public function handle($request, \Closure $next)
    {
        if( \Sentinel::check() ) 
        {
            if ($request->ajax()) 
            {
                return response()->json(['error' => 'unauthorized'], 401);
            } 
            else 
            {
                dd('Esti logat ce cauti aici?');
                // return redirect( route('admin-login') );
            }
        }
        return $next($request);
    }
}