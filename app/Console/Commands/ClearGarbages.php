<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class ClearGarbages extends BaseCommand
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'clear-garbages';

    /**
     * The console command description.
     */
    protected $description = 'Clear the orphan records from persons and memories tables';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->beforeHandle();
        $this->moment('Start clearing ... ');
        \App\Models\Memories\Memory::clearGarbage();
        \App\Models\Persons\Person::clearGarbage();
        $this->moment('End clearing ... ');
        $this->afterHandle();
       
    }
}
