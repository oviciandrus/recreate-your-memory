<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class CreateMemoryPhotos extends BaseCommand
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'create-memory-photos';

    /**
     * The console command description.
     */
    protected $description = 'Create user photos';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->beforeHandle();
        $this->moment('Start creating ... ');
        \App\Models\Memories\VApprovedMemory::chunk(100, function($memories){
            foreach($memories as $recno => $memory)
            {
                $this->moment($memory->id);
            }
        });
        $this->moment('End creating ... ');
        $this->afterHandle();
       
    }
}
