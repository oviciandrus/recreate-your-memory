<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class CreatePersonPhotos extends BaseCommand
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'create-user-photos';

    /**
     * The console command description.
     */
    protected $description = 'Create user photos';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->beforeHandle();
        $this->moment('Start creating ... ');
        \App\Models\Persons\Person::chunk(1000, function($persons){
            foreach($persons as $recno => $person)
            {
                $this->moment($person->id);
                $person->createPhoto(60, 60, 'P');
            }
        });
        $this->moment('End creating ... ');
        $this->afterHandle();
       
    }
}
