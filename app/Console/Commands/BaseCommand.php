<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Models\Commands\Command as LogCommand;

class BaseCommand extends Command
{

	protected $command = NULL;

    public function __construct()
    {
        parent::__construct();
    }

    public function now()
    {
    	return Carbon::now()->format('Y-m-d H:i:s');
    }

    protected function moment( $string = NULL)
    {
        return $this->info('[' .  $this->now() . '] ' . $string);
    }

    protected function beforeHandle()
    {
    	$this->command = new LogCommand();
        $this->command->signature = $this->signature;
        $this->command->start_at = $this->now();
    }

    protected function afterHandle()
    {
    	$this->command->end_at = $this->now();
        $this->command->save();
    }
}
