<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class CreateEdges extends BaseCommand
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'create-edges';

    /**
     * The console command description.
     */
    protected $description = 'Create edges for a memory';


    protected function createEdgesForNode($source)
    {
        $memory = \App\Models\Memories\Memory::find($source->id);
        \App\Models\Memories\Memory::chunk(1000, function($memories) use ($source){
            foreach($memories as $recno => $target)
            {
                $edge = \App\Models\Edges\Edge::createEdge($source, $target, \Config::get('rym.edge_colors'));
                if($edge)
                {
                    $source->count_edges++;
                }
                $this->moment('[' . $source->id . ',' . $target->id . ']' . ($edge ? $edge->id : '-'));
            }
        });
        $memory->count_edges = $source->count_edges;
        $memory->save();
    }

    protected function createEdges()
    {
        $memory = \App\Models\Memories\VApprovedMemory::where('count_edges', 0)->first();
        $this->moment('We will create edges for node #' . $memory->id);
        $this->createEdgesForNode($memory);
        $this->moment('End creating edges for node #' . $memory->id . ' (' . $memory->count_edges . ' edges founded)');
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->beforeHandle();
        $this->createEdges();
        $this->afterHandle();
       
    }
}
